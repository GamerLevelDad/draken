local self_ID = "tacan_bMod"

declare_plugin(self_ID,
{
installed 	 = true,
dirName	  	 = current_mod_path,
version		 = "2.5.6",		 
state		 = "installed",
info		 = _("tacan_bMod"),
developerName= _("SUNTSAG"),


Skins	=
	{
		{
			name	= _("TTS3030"),
			dir		= "Theme",
		},
	},
	
Missions =
	{
		{
			name		= _("tacan_bMod"),
			dir			= "Missions",
		},
	},		
LogBook =
	{
		{
			name		= _("tacan_bMod"),
			type		= "tacan_bMod",
		},
	},		
}
)
mount_vfs_texture_path(current_mod_path ..  "/Theme/ME")--for simulator loading window

mount_vfs_texture_path  (current_mod_path ..  "/Skins/1/ME")

dofile(current_mod_path..'/tacan_bMod.lua')
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
plugin_done()