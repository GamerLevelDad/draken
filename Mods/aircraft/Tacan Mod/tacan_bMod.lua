--(Man Portable) TTS 3030 TACAN Unit - By SUNTSAG 11/05/20

mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_liveries_path (current_mod_path.."/Liveries")
mount_vfs_texture_path  (current_mod_path.."/Textures")

WOLALIGHT_STROBES 		   = 1
WOLALIGHT_SPOTS  		   = 2
WOLALIGHT_NAVLIGHTS 	   = 3
WOLALIGHT_FORMATION_LIGHTS = 4
WOLALIGHT_TIPS_LIGHTS      = 5

tacan_bMod =  {

	Name 				=   'tacan_bMod',
	DisplayName			= _('*TACAN_b'),
	ViewSettings		= ViewSettings,

	Picture 			= "Tacan.png",
	Rate 				= 1, 
	Shape 				= "tacan_b",
		
    WorldID == WSTYPE_PLACEHOLDER,		
	-------------------------
	shape_table_data 	=
	{
		{
			file  	 = 'tacan_b';
			life  	 = 5; 
			vis   	 = 3; 
			desrt    = 'tacan_b_crush'; 
			fire  	 = { 300, 2}; 
			username = 'tacan_bMod';
			index    =  WSTYPE_PLACEHOLDER;
			classname = "lLandPlane";
			positioning = "BYNORMAL";
		},
		{
			name  = "tacan_b_crush";
			file  = "tacan_b_crush";
			fire  = { 40, 2};
		},

	},
	
	CanopyGeometry = makeAirplaneCanopyGeometry(LOOK_AVERAGE, LOOK_AVERAGE, LOOK_AVERAGE),
	
	-------------------------Attributes and Map
	mapclasskey = "P0091000064",
	attribute = {
		wsType_Air, 
		wsType_Airplane, 
		wsType_Cruiser,
		WSTYPE_PLACEHOLDER,
		"Tankers",
		"Refuelable",
	},
	
	Categories = {"{8A302789-A55D-4897-B647-66493FA6826F}", "Tanker",},
	
	------------------General  
    singleInFlight				= 	true,
	length	=	0.0,
	height	=	0.0,
	wing_area	=	0,
	wing_span	=	0,
    wing_tip_pos = 	{0.0, 0.5,	0.8},
    RCS							=	80,
	brakeshute_name	=	0,
    has_speedbrake				=	false,
    stores_number				=	0,
    tanker_type					=	0,
    is_tanker					=	true,
    refueling_points_count		=	1,
	
	refueling_points = 
    {
		--          Front/Rear, Up/Down, Left/Right
		--            +   -      +  -      -    +
        [1] = 	{ pos = {-34.535, -4.026, -18.837}, clientType = 3 },
        [2] = 	{ pos = {-34.535, -4.026,  18.806}, clientType = 3 },
    }, -- end of refueling_points

	crew_members = 
	{
		[1] = 
		{
			ejection_seat_name	=	0,
			drop_canopy_name	=	0,
			pos = 	{7.916,	0.986,	0},
		}, -- end of [1]
		[2] = 
		{
			ejection_seat_name	=	0,
			drop_canopy_name	=	0,
			pos = 	{3.949,	1.01,	0},
		}, -- end of [2]
		[3] = 
		{
			ejection_seat_name	=	0,
			drop_canopy_name	=	0,
			pos = 	{3.949,	1.01,	0},
		}, -- end of [3]
		[4] = 
		{
			ejection_seat_name	=	0,
			drop_canopy_name	=	0,
			pos = 	{3.949,	1.01,	0},
		}, -- end of [4]
	}, -- end of crew_members
    mechanimations = "Default",

	
    Picture						= "Tacan.png",
	Rate = "1",
    WingSpan = 0,
    
	-----------Fuel
	M_empty	=	4,
	M_nominal	=	10,
	M_max	=	146,
	M_fuel_max	=	0,
	H_max	=	12,
	average_fuel_consumption	=	0.1893,
	CAS_min	=	4,
	---------------------Flight Params
	V_opt	=	220,
	V_take_off	=	58,
	V_land	=	61,
	V_max_sea_level	=	280.28,
	V_max_h	=	280.28,
	Vy_max	=	10,
	Mach_max	=	0.9,
	Ny_min	=	0.5,
	Ny_max	=	2.5,
	Ny_max_e	=	2,
	AOA_take_off	=	0.14,
	bank_angle_max	=	45,
	flaps_maneuver	=	0.5,
	range	=	12247,
	
	---------------Gear
    has_differential_stabilizer	=	false,
    tand_gear_max				=	0.577,
    nose_gear_pos = 	{17.671,	0.000,	0},
    nose_gear_wheel_diameter	=	0.754,
    main_gear_pos = 	{0.00,	0.00,	0.00},
    main_gear_wheel_diameter	=	0.972,
	
	-------------------Engine
    has_afteburner				=	false,
	thrust_sum_max				=	0,
	thrust_sum_ab				=	0,
    engines_count				=	0,
    IR_emission_coeff			=	4,
    IR_emission_coeff_ab		=	0,
	
		
	------------------Sensors
    radar_can_see_ground		= true,
    detection_range_max			= 0,
	
    Sensors = {
        RWR = "Abstract RWR"
    },
	
	--------------------Radio
	TACAN = true,
	
	------------------Arms
    Pylons = {
    },
	
	Tasks = {
        aircraft_task(Refueling),
    },
    
	DefaultTask = aircraft_task(Refueling),	
	
	--------------------------Damage
	fires_pos = 
	{
		[1] = 	{-0.138,	-0.79,	0},
	}, -- end of fires_pos
	
	DamageParts = 
 	{
		[1] = "tacan_b_crush",
	},	
	
	--------------Flight
	SFM_Data =
	{
		aerodynamics = 
		{
			Cy0	=	0,
			Mzalfa	=	4.355,
			Mzalfadt	=	0.8,
			kjx	=	2.75,
			kjz	=	0.00125,
			Czbe	=	-0.016,
			cx_gear	=	0.015,
			cx_flap	=	0.05,
			cy_flap	=	1,
			cx_brk	=	0.06,
			table_data = 
			{
				[1] = 	{0,	0.023,	0.117,	0.064,	0,	0.5,	20,	1.4},
				[2] = 	{0.2,	0.023,	0.117,	0.064,	0,	1.5,	20,	1.4},
				[3] = 	{0.4,	0.023,	0.117,	0.064,	0,	2.5,	20,	1.4},
				[4] = 	{0.6,	0.025,	0.117,	0.064,	0.022,	3.5,	20,	1.4},
				[5] = 	{0.7,	0.03,	0.117,	0.083,	0.031,	3.5,	20,	1.2},
				[6] = 	{0.8,	0.032,	0.117,	0.107,	0.04,	3.5,	20,	1},
				[7] = 	{0.9,	0.045,	0.117,	0.148,	0.058,	3.5,	20,	0.8},
				[8] = 	{1,	0.054,	0.117,	0.199,	0.1,	3.5,	20,	0.7},
				[9] = 	{1.5,	0.054,	0.117,	0.199,	0.1,	3.5,	20,	0.2},
			}, -- end of table_data
		}, -- end of aerodynamics
		engine = 
		{
			Nmg	=	67.5,
			MinRUD	=	0,
			MaxRUD	=	1,
			MaksRUD	=	1,
			ForsRUD	=	1,
			type	=	"TurboJet",
			hMaxEng	=	19.5,
			dcx_eng	=	0.0085,
			cemax	=	1.24,
			cefor	=	2.56,
			dpdh_m	=	9000,
			dpdh_f	=	9000,
			table_data = 
			{
				[1] = 	{0,	373600,	373600},
				[2] = 	{0.2,	312756.6,	312756.6},
				[3] = 	{0.4,	279000,	279000},
				[4] = 	{0.6,	251000,	251000},
				[5] = 	{0.7,	253000,	253000},
				[6] = 	{0.8,	262000,	262000},
				[7] = 	{0.9,	274000,	274000},
				[8] = 	{1,	279000,	279000},
				[9] = 	{1.1,	280000,	280000},
				[10] = 	{1.2,	266000,	266000},
				[11] = 	{1.3,	95001.1,	95001.1},
			}, -- end of table_data
		}, -- end of engine
	}, -- end of [SFM_Data]
	
-- External Lights
	lights_data = 	{
		typename = "collection",
		lights = {
	        [WOLALIGHT_STROBES]   		 = {},--must be collection
	        [WOLALIGHT_SPOTS] 			 = {
				typename = "collection",
				lights = {
					{
						typename  = "spotlight" ,
						connector = "MAIN_SPOT_PTR",
						position  = {-0.173, -1.512, 2.648},
						argument  = 209,
					},
					{
						typename  = "spotlight" ,
						connector = "RESERV_SPOT_PTR",
						position  = {-0.173, -1.512, -2.648},
						argument  = 208,
					},
				},
			},
	        [WOLALIGHT_NAVLIGHTS] 		 = {
				typename = "collection",
				lights = {
					{
						typename  = "omnilight" ,
						connector = "BANO_0",
						color     = {1,1,1},
						position  = {-6.079, 2.896, 0.0},
						argument  = 192,
					},
					{
						typename  = "omnilight" ,
						connector = "BANO_1",
						color     = {0.99,0.11,0.3},
						position  = {-1.516, -0.026, -7.249},
						argument  = 190,
					},
					{
						typename  = "omnilight" ,
						connector = "BANO_2",
						color     = {0,0.894,0.6},
						position  = {-1.516, -0.026,  7.249},
						argument  = 191,
					},	
				},
			},
	        [WOLALIGHT_FORMATION_LIGHTS] = {
				typename = "collection",
				lights = {
					{typename  = "argumentlight" ,argument  = 200,},--formation_lights_tail_1 = 200;
					{typename  = "argumentlight" ,argument  = 201,},--formation_lights_tail_2 = 201;
					{typename  = "argumentlight" ,argument  = 202,},--formation_lights_left   = 202;
					{typename  = "argumentlight" ,argument  = 203,},--formation_lights_right  = 203;
					{typename  = "argumentlight" ,argument  = 88,},-- old aircraft arg 
				},
			},
		},
	},  -- end of [lights_data]	
}

add_aircraft(tacan_bMod)

