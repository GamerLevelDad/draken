
declare_plugin("L61",
{
dirName		  = current_mod_path,
displayName   = _("Juan Carlos I L61"),
shortName	  =   "Juan Carlos I L61",
version		  =   __DCS_VERSION__,
state		  =   "installed",
installed 	  = true,
developerName =   "J.Arocha and Dardo",
info		  =   _("El buque anfibio portaeronaves JUAN CARLOS I es un buque multipropósito y el mayor buque de guerra construido en España. Su denominación OTAN es LHD (Landing Helicopter Dock)"),
Skins	=
	{
		{
			name	= _("L61"),
			dir		= "Theme"
		},
	},

})
local function ship_file(f)
	if dofile(f) then
		error("can't load file "..f)
		return
	end
	if(GT) then
		GT.shape_table_data = 
		{
			{
				file  	    = GT.visual.shape;
				username    = GT.Name;
				desrt       = GT.visual.shape_dstr;
			    classname 	= "lLandShip";
				positioning = "BYNORMAL";
			},
			{
				name  = GT.visual.shape_dstr;
				file  = GT.visual.shape_dstr;
			},
		}
		add_surface_unit(GT)
		GT = nil;
	else
		error("GT empty in file "..f)
	end;
end


mount_vfs_liveries_path (current_mod_path.."/Liveries")
mount_vfs_model_path    (current_mod_path ..  "/Shapes")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/Textures.zip")
mount_vfs_texture_path (current_mod_path.."/Theme/ME")--For simulator loading window   
ship_file(current_mod_path..'/L61.lua')
ship_file(current_mod_path..'/L61s.lua')
dofile(current_mod_path.."/Database/db_units_ships.lua")

plugin_done()