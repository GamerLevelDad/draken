
GT = {};
dofile(current_mod_path..'/L61RunwaysAndRoutes.lua')
GT_t.ws = 0;
--mount_vfs_liveries_path (current_mod_path.."/Liveries")
set_recursive_metatable(GT, GT_t.generic_ship)

GT.visual = {}
GT.visual.shape = "L61"
GT.visual.shape_dstr = ""

--------------------------------------------------------------------------------
-- SHIP DESCRIPTION
--------------------------------------------------------------------------------
GT.life						= 730000;
GT.mass						= 38900000;	--7.2916e+007; 72916000
GT.max_velocity				= 15.4333;
GT.race_velocity			= 15.4333;
GT.economy_velocity			= 15.4333;
GT.economy_distance			= 7.408e+007;
GT.race_distance			= 7.408e+007;
GT.shipLength				= 233;
GT.Width					= 32;
GT.Height					= 64.36;
GT.Length					= 210; 
GT.DeckLevel				= 19.98;
GT.X_nose					= 107.196
GT.X_tail					= -100.096
GT.Tail_Width				= 31.753
GT.Gamma_max				= 0.35;
GT.Om						= 0.05;
GT.speedup					= 0.119249;
GT.R_min					= 665.8;
GT.distFindObstacles		= 1048.7;
GT.TACAN 					= true;
GT.TACAN_position 			= {-37.969, 56.996, -10.786}
GT.ICLS						= true;
GT.ICLS_Localizer_position	= {-153.0, 12.0, 9.4, 189.0}	-- {x [m], y [m], z [m], yaw [deg]}
GT.ICLS_Glideslope_position	= {-79.0, 26.5, 32.7, 3.5}	-- {x [m], y [m], z [m], glideslope = 3.5 [deg]}
GT.OLS = {
	Type = GT_t.OLS_TYPE.IFLOLS, 
	CutLightsArg = 404, 
	DatumAndWaveOffLightsArg = 405, 
	MeatBallArg = 151, 
	GlideslopeBasicAngle = 3.5, 
	VerticalCoverageAngle = 1.7
}

--------------------------------------------------------------------------------
--     X         Y          Z
-- Front/Rear, Up/Down, Left/Right
--   +   -      +  -      -    +
--------------------------------------------------------------------------------
-- Engine
GT.exhaust = {
	[1] = { size = 0.25 , pos = { 23.899, 31.926, 10.918 } },
	[2] = { size = 0.25 , pos = { 21.404, 31.926, 10.918 } },
	[3] = { size = 0.25 , pos = { 20.34, 31.926, 12.206 } },
	[4] = { size = 0.25 , pos = { 22.618, 31.926, 12.206 } },
}

-- Aviation Facilities
GT.Landing_Point	= {-71.0, 19.9, -8.0} 
GT.LSOView = {
	cockpit = "empty", 
	position = {
		--[[connector = "",]] 
		offset = {-25.0, -29.92, -4.26, 0.0, 0.0}
	}
}

-- Aircraft complement
GT.numParking		= 4;
GT.Plane_Num_		= 40;	-- Real value = 51 (45 Helicopters + 6 Harriers)
GT.Helicopter_Num_	= 36;	-- Real value = 45 Helicopters

--------------------------------------------------------------------------------
-- DAMAGE MODEL AREAS
--------------------------------------------------------------------------------
GT.DM = {
    { area_name = "NOSE_R_01", 				area_arg = 70, area_life = 150, area_fire = { connector = "FIRE_NOSE_R_01", size = 0.5}},
	{ area_name = "NOSE_R_02", 				area_arg = 94, area_life = 150, area_fire = { connector = "FIRE_NOSE_R_02", size = 0.5}},
	{ area_name = "CENTER_R_01", 			area_arg = 71, area_life = 150, area_fire = { connector = "FIRE_CENTER_R_01", size = 0.5}},
	{ area_name = "CENTER_R_02", 			area_arg = 96, area_life = 150, area_fire = { connector = "FIRE_CENTER_R_02", size = 0.5}},
    { area_name = "BACK_R", 				area_arg = 72, area_life = 300, area_fire = { connector = "FIRE_BACK_R", size = 0.5}},
	
    { area_name = "NOSE_L_01", 				area_arg = 73, area_life = 150, area_fire = { connector = "FIRE_NOSE_L_01", size = 0.5}},
	{ area_name = "NOSE_L_02", 				area_arg = 95, area_life = 150, area_fire = { connector = "FIRE_NOSE_L_02", size = 0.5}},
    { area_name = "CENTER_L_01", 			area_arg = 74, area_life = 150, area_fire = { connector = "FIRE_CENTER_L_01", size = 0.5}},
	{ area_name = "CENTER_L_02", 			area_arg = 97, area_life = 150, area_fire = { connector = "FIRE_CENTER_L_02", size = 0.5}},
	{ area_name = "CENTER_L_03", 			area_arg = 98, area_life = 150},
    { area_name = "BACK_L", 				area_arg = 75, area_life = 300, area_fire = { connector = "FIRE_BACK_L", size = 0.5}},
	
	{ area_name = "PALUBA_NOSE",			area_arg = 76, area_life = 100, area_fire = { connector = "FIRE_PALUBA_NOSE", size = 0.8}},
	{ area_name = "PALUBA_MIDLE_01",		area_arg = 77, area_life = 100, area_fire = { connector = "FIRE_PALUBA_MIDLE_01", size = 0.8}},
	{ area_name = "PALUBA_MIDLE_02",		area_arg = 92, area_life = 100, area_fire = { connector = "FIRE_PALUBA_MIDLE_02", size = 0.8}},
	{ area_name = "PALUBA_BACK_01", 		area_arg = 78, area_life = 100, area_fire = { connector = "FIRE_PALUBA_BACK_01", size = 0.8}},
	{ area_name = "PALUBA_BACK_02", 		area_arg = 93, area_life = 100, area_fire = { connector = "FIRE_PALUBA_BACK_02", size = 0.8}},
	
	{ area_name = "BACK", 					area_arg = 79, area_life = 100},
	{ area_name = "RUBKA",					area_arg = 80, area_life = 100, area_fire = { connector = "FIRE_RUBKA", size = 0.8}},
	{ area_name = "MACHTA",					area_arg = 81, area_life = 100},
	{ area_name = "TOWER",					area_arg = 82, area_life = 100},

	{ area_name = "ZA_NR",					area_arg = 99, area_life = 30},
	{ area_name = "ZA_BR",					area_arg = 100, area_life = 30},
	{ area_name = "ZA_BL",					area_arg = 101, area_life = 30},
	{ area_name = "NADSTROYKA_NR",			area_arg = 102, area_life = 30},
	{ area_name = "NADSTROYKA_BL",			area_arg = 103, area_life = 30},
	{ area_name = "NADSTROYKA_NL",			area_arg = 104, area_life = 30},
	{ area_name = "ZRK_BR",					area_arg = 105, area_life = 30},
	--{ area_name = "ZRK_NR",					area_arg = 105, area_life = 30},
	
	
	{ area_name = "LIFT_01",				area_arg = 109, area_life = 50},
	{ area_name = "LIFT_02",				area_arg = 110, area_life = 50},
	{ area_name = "LIFT_03",				area_arg = 111, area_life = 50},
	{ area_name = "LIFT_04",				area_arg = 112, area_life = 50},
}

--------------------------------------------------------------------------------
-- WEAPONS SYSTEMS
--------------------------------------------------------------------------------
-- Sensors --
GT.animation_arguments.radar1_rotation = 11; -- Radar 1 Rotation
GT.radar1_period = 3;
GT.animation_arguments.radar2_rotation = -1; -- Radar 2 Rotation (DISABLED)
GT.animation_arguments.radar3_rotation = -1; -- Radar 3 Rotation (DISABLED)
GT.animation_arguments.luna_lights = 100;

GT.WS = {}
GT.WS.maxTargetDetectionRange = 30000;
GT.WS.radar_type = 104
GT.WS.searchRadarMaxElevation = math.rad(50);

-- Weapons --
local ws = GT_t.inc_ws();

--------------------------------------------------------------------------------
-- CIWS Phalanx x 2 
-- Front Right
GT.WS[ws] = {}
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.phalanx )
GT.WS[ws].angles_mech = {
							{math.rad(180), math.rad(-180), math.rad(-5), math.rad(70)}
						}
GT.WS[ws].angles = {
					 {math.rad(10), math.rad(0), math.rad(7), math.rad(70)},
					 {math.rad(0), math.rad(-150), math.rad(-5), math.rad(70)},
					 {math.rad(-150), math.rad(10), math.rad(19), math.rad(70)}
				   }
GT.WS[ws].center = 'Gun_Center_lha_01' 
GT.WS[ws].drawArgument1 = 17
GT.WS[ws].drawArgument2 = 18
GT.WS[ws].reference_angle_Y = math.rad(0)
GT.WS[ws].LN[1].BR[1].connector_name = 'Gun_Point_lha_01';
GT.WS[ws].LN[1].fireAnimationArgument = 119;
GT.WS[ws].LN[1].createMuzzleFlashEffect = true;

-- Aft Left
ws = GT_t.inc_ws();
GT.WS[ws] = {}
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.phalanx )
GT.WS[ws].angles_mech = {
							{math.rad(180), math.rad(-180), math.rad(-5), math.rad(70)}
						}
GT.WS[ws].angles = {
					 {math.rad(-135), math.rad(-140), math.rad(-5), math.rad(45)},
					 {math.rad(-140), math.rad(45), math.rad(-5), math.rad(70)},
				   }
GT.WS[ws].center = 'Gun_Point_lha_02'
GT.WS[ws].drawArgument1 = 19
GT.WS[ws].drawArgument2 = 20
GT.WS[ws].reference_angle_Y = math.rad(180)
GT.WS[ws].LN[1].BR[1].connector_name = 'POINT_GUN_ZA_BL';
-- GT.WS[ws].LN[1].BR[1].connector_name = 'Gun_Point_lha_02';
-- GT.WS[ws].LN[1].BR[1].connector_name = 'Gun_Center_lha_02';
GT.WS[ws].LN[1].fireAnimationArgument = 120;
GT.WS[ws].LN[1].createMuzzleFlashEffect = true;

--------------------------------------------------------------------------------
-- Ship's SAMs: RAM System x 2
-- Mk 95 radars
ws = GT_t.inc_ws();
local first_MK95_tracker_id = ws;
GT.WS[ws] = {
	omegaY = 1,
	omegaZ = 1,
	pidY = {p=100, i=0.05, d=12, inn = 50},
	pidZ = {p=100, i=0.05, d=12, inn = 50},
	area = 'RUBKA',
	center = 'CENTER_RADAR_9',--POINT CONNECTOR NAME REQUIRED.... NEEDS TO BE PLACED AT CENTER POINT OF THE REAR FLAT SLAB RADAR
	drawArgument1 = 6,
	drawArgument2 = 7,
	reference_angle_Y = math.rad(90),
	angles = { 
				{math.rad(180), math.rad(-90), math.rad(-90), math.rad(80)}
			},
	LN = {
		[1] = {
			type = 102,
			frequencyRange = {0.5e9, 0.58e9},
			distanceMin = 400,
			distanceMax = 30000,
			reactionTime = 2.5,
			reflection_limit = 0.1,
			ECM_K = 0.65,
			min_trg_alt = 5,
			max_trg_alt = 15000,
			max_number_of_missiles_channels = 1,
			beamWidth = math.rad(90);
		}
	}
};
local MK95_TRACKERS = {{{'self', ws}}}

ws = GT_t.inc_ws();
GT.WS[ws] = {};
set_recursive_metatable(GT.WS[ws], GT.WS[first_MK95_tracker_id]);
GT.WS[ws].center = 'CENTER_RADAR_10'--POINT CONNECTOR NAME REQUIRED.... NEEDS TO BE PLACED AT CENTER POINT OF THE REAR DISH RADAR
GT.WS[ws].drawArgument1 = 8
GT.WS[ws].drawArgument2 = 9
GT.WS[ws].reference_angle_Y = math.rad(-90)
GT.WS[ws].angles = {
						{math.rad(90), math.rad(-170), math.rad(-90), math.rad(80)},
						{math.rad(-170), math.rad(170), math.rad(-90), math.rad(80)},
					}
table.insert(MK95_TRACKERS, {{'self', ws}})

--------------------------------------------------------------------------------
-- Mk 57 Mod3 Sea Sparrow (RAM system is unavailable)
-- Forward right
ws = GT_t.inc_ws();
GT.WS[ws] = {}
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.seasparrow)
GT.WS[ws].area = 'NADSTROYKA_NR'
GT.WS[ws].center = 'RAM_Center_lha_01'--POINT CONNECTOR NAME REQUIRED.... NEEDS TO BE PLACED AT CENTER POINT OF THE FRONT SAM LAUNCHER 
GT.WS[ws].angles_mech = {
							{math.rad(180), math.rad(-180), math.rad(-5), math.rad(85)}
						}
GT.WS[ws].angles = {
					 {math.rad(180), math.rad(40), math.rad(45), math.rad(80)},
					 {math.rad(40), math.rad(-170), math.rad(-5), math.rad(80)},
				   }
GT.WS[ws].drawArgument1 = 24
GT.WS[ws].drawArgument2 = 25
GT.WS[ws].reference_angle_Y = math.rad(0)
GT.WS[ws].LN[1].min_launch_angle = math.rad(35);
GT.WS[ws].LN[1].BR = {
						{connector_name = 'Rocket_Point_01', drawArgument = 188},
						{connector_name = 'Rocket_Point_02', drawArgument = 189},
						{connector_name = 'Rocket_Point_03', drawArgument = 190},
						{connector_name = 'Rocket_Point_04', drawArgument = 191},
						{connector_name = 'Rocket_Point_05', drawArgument = 192},
						{connector_name = 'Rocket_Point_06', drawArgument = 193},
						{connector_name = 'Rocket_Point_07', drawArgument = 194},
						{connector_name = 'Rocket_Point_08', drawArgument = 195}
					}
GT.WS[ws].LN[1].depends_on_unit = MK95_TRACKERS

-- Aft left
ws = GT_t.inc_ws();
GT.WS[ws] = {}
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.seasparrow)
GT.WS[ws].area = 'NADSTROYKA_BL'
GT.WS[ws].center = 'RAM_Center_lha_02'--POINT CONNECTOR NAME REQUIRED.... NEEDS TO BE PLACED AT CENTER POINT OF THE BACK RIGHT SAM LAUNCHER
GT.WS[ws].angles_mech = {
							{math.rad(180), math.rad(-180), math.rad(-5), math.rad(95)}
						}
GT.WS[ws].angles = {
					 {math.rad(-165), math.rad(45), math.rad(-5), math.rad(80)},
					 {math.rad(45), math.rad(0), math.rad(10), math.rad(80)},
				   }
GT.WS[ws].drawArgument1 = 26
GT.WS[ws].drawArgument2 = 27
GT.WS[ws].reference_angle_Y = math.rad(180)
GT.WS[ws].LN[1].min_launch_angle = math.rad(35);
GT.WS[ws].LN[1].BR = {
						{connector_name = 'Rocket_Point_20', drawArgument = 188},
						{connector_name = 'Rocket_Point_21', drawArgument = 189},
						{connector_name = 'Rocket_Point_22', drawArgument = 190},
						{connector_name = 'Rocket_Point_23', drawArgument = 191},
						{connector_name = 'Rocket_Point_24', drawArgument = 192},
						{connector_name = 'Rocket_Point_25', drawArgument = 193},
						{connector_name = 'Rocket_Point_26', drawArgument = 194},
						{connector_name = 'Rocket_Point_27', drawArgument = 195}
					}
GT.WS[ws].LN[1].depends_on_unit = MK95_TRACKERS

--------------------------------------------------------------------------------
-- SHIP VALUES
--------------------------------------------------------------------------------
GT.Name			= "L61"
GT.DisplayName	= _("L61 Juan Carlos I")
GT.Rate			= 5500.000000;

GT.Sensors = {  
				OPTIC = {"long-range naval optics", "long-range naval LLTV"},
                RADAR = {"seasparrow tr","carrier search radar",},
            };

GT.airFindDist		= 300000;	-- Max detenction range air threats (meters)
GT.airWeaponDist	= 150000;	-- Max engagement range air threats (meters)

GT.DetectionRange	= GT.airFindDist;
GT.ThreatRange		= GT.airWeaponDist;
GT.Singleton		= "yes";
GT.mapclasskey		= "P0091000067";
GT.attribute		= {wsType_Navy, wsType_Ship, wsType_AirCarrier, WSTYPE_PLACEHOLDER,
						"Aircraft Carriers",
						"ski_jump",
						"RADAR_BAND1_FOR_ARM",
						"RADAR_BAND2_FOR_ARM",
						"DetectionByAWACS",
					};
GT.Categories		= {
						{name = "AircraftCarrier"},
						{name = "AircraftCarrier With Tramplin"},
						{name = "Armed Ship"}
					};

--GT.Countries		= { "SPN",};
--------------------------------------------------------------------------------
add_surface_unit(GT)
