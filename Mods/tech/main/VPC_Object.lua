mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_texture_path  (current_mod_path.."/Textures/VPCtextures")
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-CZT-2.EDM"
	CWAY.visual.shape_dstr = "VP-CZT-2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-CZT-2"
	CWAY.DisplayName = _("VP-CZT-2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-CZT-1.EDM"
	CWAY.visual.shape_dstr = "VP-CZT-1.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-CZT-1"
	CWAY.DisplayName = _("VP-CZT-1")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-CZT-3.EDM"
	CWAY.visual.shape_dstr = "VP-CZT-3.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-CZT-3"
	CWAY.DisplayName = _("VP-CZT-3")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-CZT KAPONIR.EDM"
	CWAY.visual.shape_dstr = "VP-CZT KAPONIR.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-CZT KAPONIR"
	CWAY.DisplayName = _("VP-CZT KAPONIR")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ELECTRIC SUBSTATION.EDM"
	CWAY.visual.shape_dstr = "VP-ELECTRIC SUBSTATION.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-ELECTRIC SUBSTATION"
	CWAY.DisplayName = _("VP-ELECTRIC SUBSTATION")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-STREETLIGHT.EDM"
	CWAY.visual.shape_dstr = "VP-STREETLIGHT.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-STREETLIGHT"
	CWAY.DisplayName = _("VP-STREETLIGHT")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-FIREHOUSE.EDM"
	CWAY.visual.shape_dstr = "VP-FIREHOUSE.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-FIREHOUSE"
	CWAY.DisplayName = _("VP-FIREHOUSE")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-BIG ANGAR.EDM"
	CWAY.visual.shape_dstr = "VP-BIG ANGAR.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-BIG ANGAR"
	CWAY.DisplayName = _("VP-BIG ANGAR")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-KDP.EDM"
	CWAY.visual.shape_dstr = "VP-KDP.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-KDP"
	CWAY.DisplayName = _("VP-KDP")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-KDP 2.EDM"
	CWAY.visual.shape_dstr = "VP-KDP 2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-KDP 2"
	CWAY.DisplayName = _("VP-KDP 2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-SKP.EDM"
	CWAY.visual.shape_dstr = "VP-SKP.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-SKP"
	CWAY.DisplayName = _("VP-SKP")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-HOUSE.EDM"
	CWAY.visual.shape_dstr = "VP-HOUSE.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-HOUSE"
	CWAY.DisplayName = _("VP-HOUSE")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-HOUSE ADMIN.EDM"
	CWAY.visual.shape_dstr = "VP-HOUSE ADMIN.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-HOUSE ADMIN"
	CWAY.DisplayName = _("VVP-HOUSE ADMIN")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-HOUSE COMPLEX.EDM"
	CWAY.visual.shape_dstr = "VP-HOUSE COMPLEX.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-HOUSE COMPLEX"
	CWAY.DisplayName = _("VP-HOUSE COMPLEX")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ARZ.EDM"
	CWAY.visual.shape_dstr = "VP-ARZ.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-ARZ"
	CWAY.DisplayName = _("VP-ARZ")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ARZ 2.EDM"
	CWAY.visual.shape_dstr = "VP-ARZ 2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-ARZ 2"
	CWAY.DisplayName = _("VP-ARZ 2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ARZ 2 CLOSED.EDM"
	CWAY.visual.shape_dstr = "VP-ARZ 2 CLOSED.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-ARZ 2 CLOSED"
	CWAY.DisplayName = _("VP-ARZ 2 CLOSED")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ARZ (Mozdok Mission).EDM"
	CWAY.visual.shape_dstr = "VP-ARZ (Mozdok Mission).EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-ARZ (Mozdok Mission)"
	CWAY.DisplayName = _("VP-ARZ (Mozdok Mission)")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-PROJECTOR.EDM"
	CWAY.visual.shape_dstr = "VP-PROJECTOR.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-PROJECTOR"
	CWAY.DisplayName = _("VP-PROJECTOR")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-LIGHTING TOWER.EDM"
	CWAY.visual.shape_dstr = "VP-LIGHTING TOWER.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-LIGHTING TOWER"
	CWAY.DisplayName = _("VP-LIGHTING TOWER")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-GOTB_750m.EDM"
	CWAY.visual.shape_dstr = "VP-GOTB_750m.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-GOTB_750m"
	CWAY.DisplayName = _("VP-GOTB_750m")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-GOTB (Only Mozdok Mission).EDM"
	CWAY.visual.shape_dstr = "VP-GOTB (Only Mozdok Mission).EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-GOTB (Only Mozdok Mission)"
	CWAY.DisplayName = _("VP-GOTB (Only Mozdok Mission)")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-AIRSHIP.EDM"
	CWAY.visual.shape_dstr = "VP-AIRSHIP.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-AIRSHIP"
	CWAY.DisplayName = _("VP-AIRSHIP")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-SU35 DAMAGE.EDM"
	CWAY.visual.shape_dstr = "VP-SU35 DAMAGE.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-SU35 DAMAGE"
	CWAY.DisplayName = _("VP-SU35 DAMAGE")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000024";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-RZONA.EDM"
	CWAY.visual.shape_dstr = "VP-RZONA.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VP-RZONA"
	CWAY.DisplayName = _("VP-RZONA")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- Part 2

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ANGAR.EDM"
	CWAY.visual.shape_dstr = "VP-ANGAR.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-ANGAR"
	CWAY.DisplayName = _("VP-ANGAR")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ANGAR 2.EDM"
	CWAY.visual.shape_dstr = "VP-ANGAR 2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-ANGAR 2"
	CWAY.DisplayName = _("VP-ANGAR 2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-BARRIER.EDM"
	CWAY.visual.shape_dstr = "VP-BARRIER.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-BARRIER"
	CWAY.DisplayName = _("VP-BARRIER")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-FIRE_EX.EDM"
	CWAY.visual.shape_dstr = "VP-FIRE_EX.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-FIRE_EX"
	CWAY.DisplayName = _("VP-FIRE_EX")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-FLAG.EDM"
	CWAY.visual.shape_dstr = "VP-FLAG.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-FLAG"
	CWAY.DisplayName = _("VP-FLAG")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-GOTB_100m.EDM"
	CWAY.visual.shape_dstr = "VP-GOTB_100m.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-GOTB_100m"
	CWAY.DisplayName = _("VP-GOTB_100m")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-GOTB_250m.EDM"
	CWAY.visual.shape_dstr = "VP-GOTB_250m.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-GOTB_250m"
	CWAY.DisplayName = _("VP-GOTB_250m")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-MAN_1_Idle.EDM"
	CWAY.visual.shape_dstr = "VP-MAN_1_Idle.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-MAN_1_Idle"
	CWAY.DisplayName = _("VP-MAN_1_Idle")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-MAN_2_Check.EDM"
	CWAY.visual.shape_dstr = "VP-MAN_2_Check.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-MAN_2_Check"
	CWAY.DisplayName = _("VP-MAN_2_Check")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-MAN_2_Idle.EDM"
	CWAY.visual.shape_dstr = "VP-MAN_2_Idle.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-MAN_2_Idle"
	CWAY.DisplayName = _("VP-MAN_2_Idle")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-MONUMENT-LA-7.EDM"
	CWAY.visual.shape_dstr = "VP-MONUMENT-LA-7.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-MONUMENT-LA-7"
	CWAY.DisplayName = _("VP-MONUMENT-LA-7")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-R27.EDM"
	CWAY.visual.shape_dstr = "VP-R27.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-R27"
	CWAY.DisplayName = _("VP-R27")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-STAIR_1.EDM"
	CWAY.visual.shape_dstr = "VP-STAIR_1.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-STAIR_1"
	CWAY.DisplayName = _("VP-STAIR_1")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-STAIR_2.EDM"
	CWAY.visual.shape_dstr = "VP-STAIR_2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-STAIR_2"
	CWAY.DisplayName = _("VP-STAIR_2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-T-50.EDM"
	CWAY.visual.shape_dstr = "VP-T-50.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-T-50"
	CWAY.DisplayName = _("VP-T-50")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000024";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-T-50_CLOTH.EDM"
	CWAY.visual.shape_dstr = "VP-T-50_CLOTH.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-T-50_CLOTH"
	CWAY.DisplayName = _("VP-T-50_CLOTH")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000024";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-VODILO.EDM"
	CWAY.visual.shape_dstr = "VP-VODILO.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VP-VODILO"
	CWAY.DisplayName = _("VP-VODILO")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "rd_1.EDM"
	CWAY.visual.shape_dstr = "rd_1.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1   
	
        CWAY.Name = "rd_1"
	CWAY.DisplayName = _("rd_1")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "rd_2.EDM"
	CWAY.visual.shape_dstr = "rd_2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "rd_2"
	CWAY.DisplayName = _("rd_2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "rd_3.EDM"
	CWAY.visual.shape_dstr = "rd_3.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "rd_3"
	CWAY.DisplayName = _("rd_3")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "rd_4.EDM"
	CWAY.visual.shape_dstr = "rd_4.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "rd_4"
	CWAY.DisplayName = _("rd_4")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "rd_5.EDM"
	CWAY.visual.shape_dstr = "rd_5.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "rd_5"
	CWAY.DisplayName = _("rd_5")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "vpp_08.EDM"
	CWAY.visual.shape_dstr = "vpp_08.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "vpp_08"
	CWAY.DisplayName = _("vpp_08")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "vpp_26.EDM"
	CWAY.visual.shape_dstr = "vpp_26.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "vpp_26"
	CWAY.DisplayName = _("vpp_26")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-BOGIE OFAB 120.EDM"
	CWAY.visual.shape_dstr = "VP-BOGIE OFAB 120.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-BOGIE OFAB 120"
	CWAY.DisplayName = _("VP-BOGIE OFAB 120")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-BOGIE ODAB 500.EDM"
	CWAY.visual.shape_dstr = "VP-BOGIE ODAB 500.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-BOGIE ODAB 500"
	CWAY.DisplayName = _("VP-BOGIE ODAB 500")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-SU35 CAR.EDM"
	CWAY.visual.shape_dstr = "VP-SU35 CAR.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-SU35 CAR"
	CWAY.DisplayName = _("VP-SU35 CAR")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000024";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-SU35 Stair.EDM"
	CWAY.visual.shape_dstr = "VP-SU35 Stair.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-SU35 Stair"
	CWAY.DisplayName = _("VP-SU35 Stair")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000024";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-SU35 Cloth.EDM"
	CWAY.visual.shape_dstr = "VP-SU35 Cloth.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-SU35 Cloth"
	CWAY.DisplayName = _("VP-SU35 Cloth")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000024";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-SU 24.EDM"
	CWAY.visual.shape_dstr = "VP-SU 24.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-SU 24"
	CWAY.DisplayName = _("VP-SU 24")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000024";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ZIL RADAR.EDM"
	CWAY.visual.shape_dstr = "VP-ZIL RADAR.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-ZIL RADAR"
	CWAY.DisplayName = _("VP-ZIL RADAR")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-URAL APA.EDM"
	CWAY.visual.shape_dstr = "VP-URAL APA.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-URAL АПА"
	CWAY.DisplayName = _("VP-URAL АПА")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-URAL APA_2.EDM"
	CWAY.visual.shape_dstr = "VP-URAL APA_2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-URAL АПА 2"
	CWAY.DisplayName = _("VP-URAL АПА 2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-URAL TENT.EDM"
	CWAY.visual.shape_dstr = "VP-URAL TENT.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-URAL TENT"
	CWAY.DisplayName = _("VP-URAL TENT")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-CONTAINER.EDM"
	CWAY.visual.shape_dstr = "VP-CONTAINER.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-CONTAINER"
	CWAY.DisplayName = _("VP-CONTAINER")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-END_RUNWAY.EDM"
	CWAY.visual.shape_dstr = "VP-END_RUNWAY.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-END_RUNWAY"
	CWAY.DisplayName = _("VP-END_RUNWAY")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-GOTB_Metall_by_LEVA.EDM"
	CWAY.visual.shape_dstr = "VP-GOTB_Metall_by_LEVA.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-GOTB_Metall_by_LEVA"
	CWAY.DisplayName = _("VP-GOTB_Metall_by_LEVA")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-BARRACKS.EDM"
	CWAY.visual.shape_dstr = "VP-BARRACKS.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
    CWAY.animation_arguments.headlights = 1    
	
        CWAY.Name = "VP-BARRACKS"
	CWAY.DisplayName = _("VP-BARRACKS")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-ZABOR 200m.EDM"
	CWAY.visual.shape_dstr = "VP-ZABOR 200m.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;    
	
        CWAY.Name = "VP-ZABOR 200m"
	CWAY.DisplayName = _("VP-ZABOR 200m")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-STOLBIK.EDM"
	CWAY.visual.shape_dstr = "VP-STOLBIK.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VP-STOLBIK"
	CWAY.DisplayName = _("VP-STOLBIK")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-FOREST.EDM"
	CWAY.visual.shape_dstr = "VP-FOREST.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VP-FOREST"
	CWAY.DisplayName = _("VP-FOREST")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VP-AZT-60.EDM"
	CWAY.visual.shape_dstr = "VP-AZT-60.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VP-AZT-60"
	CWAY.DisplayName = _("VP-AZT-60")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
