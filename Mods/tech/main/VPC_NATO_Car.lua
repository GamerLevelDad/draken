mount_vfs_model_path	(current_mod_path.."/Shapes NATO")
mount_vfs_texture_path  (current_mod_path.."/Textures NATO/VPC_NATO_textures")

-- LOADER

GT_t.CH_t.LOADER = {
	life = 1.5,
	mass = 3990,
	length = 4.72,
	width = 2.18,
	max_road_velocity = 31.3889,
	max_slope = 0.27,
	engine_power = 160,
	gear_count = 4,
	canSwim = false,
	canWade = true,
	max_vert_obstacle = 0.56,
	max_acceleration = 2.925926,
	min_turn_radius = 7.62,
	X_gear_1 = 1.925,
	Y_gear_1 = 0,
	Z_gear_1 = 0.888,
	X_gear_2 = -1.361,
	Y_gear_2 = 0,
	Z_gear_2 = 0.888,
	gear_type = GT_t.GEAR_TYPES.WHEELS,
	r_max = 0.46,
	armour_thickness = 0.005,
}

GT = {};
set_recursive_metatable(GT, GT_t.generic_wheel_vehicle);
set_recursive_metatable(GT.chassis, GT_t.CH_t.LOADER);

GT.visual.shape = "LOADER";
GT.visual.shape_dstr = "wasp_d1";

GT.turbine = true;
GT.animation_arguments.stoplights = 30;
GT.animation_arguments.headlights = 31;
GT.animation_arguments.markerlights = 32;
--chassis
GT.swing_on_run = false;

--Burning after hit
GT.visual.fire_size = 0.6 --relative burning size
GT.visual.fire_pos[1] = 1 -- center of burn at long axis shift(meters)
GT.visual.fire_pos[2] = 0 -- center of burn shift at vertical shift(meters)
GT.visual.fire_pos[3] = 0 -- center of burn at transverse axis shift(meters)
GT.visual.fire_time = 900 --burning time (seconds)
GT.animation_arguments.crew_presence = 50;

GT.Name = "LOADER";
GT.Aliases = {"LOADER"};
GT.DisplayName = _("VPC-NATO-CAR LOADER");
GT.Rate = 3;

GT.EPLRS = true
--GT.driverViewPoint = {0.2, 1.5, -0.7};
GT.driverViewConnectorName = "DRIVER_POINT"
GT.driverCockpit = "DriverCockpit/DriverCockpitWithIR";

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000005";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_Hummer,
                "APC", "Datalink",
                "human_vehicle",
                };
GT.category = "Unarmed";
add_surface_unit(GT)


-- FORD150

GT_t.CH_t.FORD150 = {
	life = 1.5,
	mass = 3990,
	length = 4.72,
	width = 2.18,
	max_road_velocity = 31.3889,
	max_slope = 0.27,
	engine_power = 160,
	gear_count = 4,
	canSwim = false,
	canWade = true,
	max_vert_obstacle = 0.56,
	max_acceleration = 2.925926,
	min_turn_radius = 7.62,
	X_gear_1 = 1.925,
	Y_gear_1 = 0,
	Z_gear_1 = 0.888,
	X_gear_2 = -1.361,
	Y_gear_2 = 0,
	Z_gear_2 = 0.888,
	gear_type = GT_t.GEAR_TYPES.WHEELS,
	r_max = 0.46,
	armour_thickness = 0.005,
}

GT = {};
set_recursive_metatable(GT, GT_t.generic_wheel_vehicle);
set_recursive_metatable(GT.chassis, GT_t.CH_t.FORD150);

GT.visual.shape = "FORD150";
GT.visual.shape_dstr = "wasp_d1";

GT.turbine = true;
GT.animation_arguments.stoplights = 30;
GT.animation_arguments.headlights = 31;
GT.animation_arguments.markerlights = 32;
--chassis
GT.swing_on_run = false;

--Burning after hit
GT.visual.fire_size = 0.6 --relative burning size
GT.visual.fire_pos[1] = 1 -- center of burn at long axis shift(meters)
GT.visual.fire_pos[2] = 0 -- center of burn shift at vertical shift(meters)
GT.visual.fire_pos[3] = 0 -- center of burn at transverse axis shift(meters)
GT.visual.fire_time = 900 --burning time (seconds)
GT.animation_arguments.crew_presence = 50;

GT.Name = "FORD150";
GT.Aliases = {"FORD150"};
GT.DisplayName = _("VPC-NATO-FORD150");
GT.Rate = 3;

GT.EPLRS = true
--GT.driverViewPoint = {0.2, 1.5, -0.7};
GT.driverViewConnectorName = "DRIVER_POINT"
GT.driverCockpit = "DriverCockpit/DriverCockpitWithIR";

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000005";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_Hummer,
                "APC", "Datalink",
                "human_vehicle",
                };
GT.category = "Unarmed";
add_surface_unit(GT)


