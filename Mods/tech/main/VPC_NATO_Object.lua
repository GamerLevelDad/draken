mount_vfs_model_path	(current_mod_path.."/Shapes NATO")
mount_vfs_texture_path  (current_mod_path.."/Textures NATO/VPC_NATO_textures")

-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-OXYGEN.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-OXYGEN.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-OXYGEN"
	CWAY.DisplayName = _("VPC-NATO-OXYGEN")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)



-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-CASE.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-CASE.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-CASE"
	CWAY.DisplayName = _("VPC-NATO-CASE")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-CANOPY v2.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-CANOPY v2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	     CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VPC-NATO-CANOPY v2"
	CWAY.DisplayName = _("VPC-NATO-CANOPY v2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-CANOPY v3.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-CANOPY v3.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	     CWAY.animation_arguments.headlights = 1;
	
        CWAY.Name = "VPC-NATO-CANOPY v3"
	CWAY.DisplayName = _("VPC-NATO-CANOPY v3")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-GAS BARREL.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-GAS BARREL.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-GAS BARREL"
	CWAY.DisplayName = _("VPC-NATO-GAS BARREL")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-GOTB.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-GOTB.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-GOTB"
	CWAY.DisplayName = _("VPC-NATO-GOTB")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-PROTECTION.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-PROTECTION.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-PROTECTION"
	CWAY.DisplayName = _("VPC-NATO-PROTECTION")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

	


-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-GENERATOR"
	CWAY.visual.shape_dstr = "VPC-NATO-GENERATOR"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-GENERATOR"
	CWAY.DisplayName = _("VPC-NATO-GENERATOR")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-GENERATOR v2"
	CWAY.visual.shape_dstr = "VPC-NATO-GENERATOR v2"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-GENERATOR v2"
	CWAY.DisplayName = _("VPC-NATO-GENERATOR v2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	

	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-A10-CH"
	CWAY.visual.shape_dstr = "VPC-NATO-A10-CH"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-A10-CH"
	CWAY.DisplayName = _("VPC-NATO-A10-CH")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-AGM-65 CONTAINERS"
	CWAY.visual.shape_dstr = "VPC-NATO-AGM-65 CONTAINERS"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-AGM-65 CONTAINERS"
	CWAY.DisplayName = _("VPC-NATO-AGM-65 CONTAINERS")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-AGM-65 D"
	CWAY.visual.shape_dstr = "VPC-NATO-AGM-65 D"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-AGM-65 D"
	CWAY.DisplayName = _("VPC-NATO-AGM-65 D")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-AGM-65 E"
	CWAY.visual.shape_dstr = "VPC-NATO-AGM-65 E"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-AGM-65 E"
	CWAY.DisplayName = _("VPC-NATO-AGM-65 E")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-AGM-88 CART"
	CWAY.visual.shape_dstr = "VPC-NATO-AGM-88 CART"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-AGM-88 CART"
	CWAY.DisplayName = _("VPC-NATO-AGM-88 CART")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-AIM-9M CART"
	CWAY.visual.shape_dstr = "VPC-NATO-AIM-9M CART"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-AIM-9M CART"
	CWAY.DisplayName = _("VPC-NATO-AIM-9M CART")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-FORD 150 CART"
	CWAY.visual.shape_dstr = "VPC-NATO-FORD 150 CART"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-FORD 150 CART"
	CWAY.DisplayName = _("VPC-NATO-FORD 150 CART")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-GBU-31 CART"
	CWAY.visual.shape_dstr = "VPC-NATO-GBU-31 CART"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-GBU-31 CART"
	CWAY.DisplayName = _("VPC-NATO-GBU-31 CART")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-BALON"
	CWAY.visual.shape_dstr = "VPC-NATO-BALON"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;  
	
        CWAY.Name = "VPC-NATO-BALON"
	CWAY.DisplayName = _("VPC-NATO-BALON")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-CANOPY"
	CWAY.visual.shape_dstr = "VPC-NATO-CANOPY"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "VPC-NATO-CANOPY"
	CWAY.DisplayName = _("VPC-NATO-CANOPY")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-CAR LOADER empty"
	CWAY.visual.shape_dstr = "VPC-NATO-CAR LOADER empty"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "VPC-NATO-CAR LOADER empty"
	CWAY.DisplayName = _("VPC-NATO-CAR LOADER empty")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-LIGHT TOWER v1"
	CWAY.visual.shape_dstr = "VPC-NATO-LIGHT TOWER v1"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "VPC-NATO-LIGHT TOWER v1"
	CWAY.DisplayName = _("VPC-NATO-LIGHT TOWER v1")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-CONTROL TOWER"
	CWAY.visual.shape_dstr = "VPC-NATO-CONTROL TOWER"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "VPC-NATO-CONTROL TOWER"
	CWAY.DisplayName = _("VPC-NATO-CONTROL TOWER")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-LIGHT TOWER v2"
	CWAY.visual.shape_dstr = "VPC-NATO-LIGHT TOWER v2"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "VPC-NATO-LIGHT TOWER v2"
	CWAY.DisplayName = _("VPC-NATO-LIGHT TOWER v2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-LIGHT TOWER v3"
	CWAY.visual.shape_dstr = "VPC-NATO-LIGHT TOWER v3"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-LIGHT TOWER v3"
	CWAY.DisplayName = _("VPC-NATO-LIGHT TOWER v3")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-HOUSE 1"
	CWAY.visual.shape_dstr = "VPC-NATO-HOUSE 1"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-HOUSE 1"
	CWAY.DisplayName = _("VPC-NATO-HOUSE 1")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-HOUSE 2"
	CWAY.visual.shape_dstr = "VPC-NATO-HOUSE 2"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-HOUSE 2"
	CWAY.DisplayName = _("VPC-NATO-HOUSE 2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-TECH Open"
	CWAY.visual.shape_dstr = "VPC-NATO-TECH Open"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-TECH Open"
	CWAY.DisplayName = _("VPC-NATO-TECH Open")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-TECH Closed"
	CWAY.visual.shape_dstr = "VPC-NATO-TECH Closed"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-TECH Closed"
	CWAY.DisplayName = _("VPC-NATO-TECH Closed")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-FIREHOUSE"
	CWAY.visual.shape_dstr = "VPC-NATO-FIREHOUSE"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-FIREHOUSE"
	CWAY.DisplayName = _("VPC-NATO-FIREHOUSE")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-DUMPSTER v1"
	CWAY.visual.shape_dstr = "VPC-NATO-DUMPSTER v1"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-DUMPSTER v1"
	CWAY.DisplayName = _("VPC-NATO-DUMPSTER v1")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-DUMPSTER v2"
	CWAY.visual.shape_dstr = "VPC-NATO-DUMPSTER v2"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
		
		
	
        CWAY.Name = "VPC-NATO-DUMPSTER v2"
	CWAY.DisplayName = _("VPC-NATO-DUMPSTER v2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)	