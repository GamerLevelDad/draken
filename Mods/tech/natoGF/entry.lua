local self_ID = "NATO Ground Forces and units"

declare_plugin(
    self_ID,
    {
		installed 	 = true, 
		dirName	  	 = current_mod_path,
		fileMenuName = _("NATO Ground Forces"),
		shortName	 = "NATO Ground Forces",
		version		 = "NATO Ground Forces",		 
		state		 = "installed",
		info		 = _("NATO Ground Forces"),

		
		Skins = {
            {
                name = _("NATO Ground Forces"),
                dir = "Skins/1"
            },
        },
		
		encyclopedia_path = current_mod_path .. '/Encyclopedia',
})
----------------------------------------------------------------------------------------
mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_liveries_path (current_mod_path.."/Liveries/")
mount_vfs_texture_path (current_mod_path.."/Textures/")

dofile(current_mod_path..'/NATO_Forces.lua')
dofile(current_mod_path..'/SovietIran_Forces.lua')
dofile(current_mod_path..'/NATO_Forces_TARAWA.lua')
dofile(current_mod_path..'/NATO_Forces_STENNIS.lua')
dofile(current_mod_path..'/NATO_Forces_OILTANKER.lua')
dofile(current_mod_path..'/SovietIran_Forces_KUZNETZOV.lua')
dofile(current_mod_path..'/SovietIran_Forces_OILTANKER.lua')
dofile(current_mod_path..'/NATO_Forces_HERMES.lua')

mount_vfs_texture_path(current_mod_path .. "/Skins/1/ME")
plugin_done()

--NATO Ground Forces and Units Mod - By SUNTSAG--