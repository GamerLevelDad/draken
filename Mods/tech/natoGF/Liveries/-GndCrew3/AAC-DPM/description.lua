
livery = 
{
    
    {"Bodymat", 0, "nato_1_body_diffuse", true};
    {"Bodymat", 1, "nato_1_body_normal", true};
    {"Shoesmat", 0, "RAF_1_shoes_diffuse", false};
    {"Shoesmat", 1, "nato_1_shoes_normal", true};
    {"Topmat", 0, "AAC_1_top_diffuse", false};
    {"Topmat", 1, "nato_1_top_normal", true};
    {"Glovemat", 0, "nato_1_glove_diffuse", true};
    {"Glovemat", 1, "nato_1_glove_normal", true};
    {"Bottommat", 0, "RAF_1_bottom_diffuse", false};
    {"Bottommat", 1, "nato_1_bottom_normal", true};
    {"Hatmat", 0, "RAF_1_hat_diffuse", false};
    {"Hatmat", 1, "nato_1_hat_normal", true};
   
}
name = "AAC-DPM"


