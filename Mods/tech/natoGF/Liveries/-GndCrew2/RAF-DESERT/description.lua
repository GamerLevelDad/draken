
livery = 
{
    
    {"Bodymat", 0, "nato_2_body_diffuse", true};
    {"Bodymat", 1, "nato_2_body_normal", true};
    {"Shoesmat", 0, "nato_1_shoes_diffuse", true};
    {"Shoesmat", 1, "nato_1_shoes_normal", true};
    {"Topmat", 0, "RAFD_2_top_diffuse", false};
    {"Topmat", 1, "nato_2_top_normal", true};
    {"Glovemat", 0, "nato_2_glove_diffuse", true};
    {"Glovemat", 1, "nato_2_glove_normal", true};
    {"Bottommat", 0, "RAFD_2_bottom_diffuse", false};
    {"Bottommat", 1, "nato_2_bottom_normal", true};
    {"Hatmat", 0, "tex_hat_diffuse", true};
    {"Hatmat", 1, "tex_hat_normal 1", true,};
    {"Eyewearmat", 0, "nato_2_eyewear_diffuse", true};
    {"Eyewearmat", 1, "nato_2_eyewear_normal", true};
    {"Eyewearmat", 11, "nato_2_eyewear_specular", true};
}
name = "RAF-DESERT"


