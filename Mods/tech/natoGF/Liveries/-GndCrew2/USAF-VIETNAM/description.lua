
livery = 
{
    
    {"Bodymat", 0, "nato_2_body_diffuse", true};
    {"Bodymat", 1, "nato_2_body_normal", true};
    {"Shoesmat", 0, "RAF_2_shoes_diffuse", false};
    {"Shoesmat", 1, "nato_2_shoes_normal", true};
    {"Topmat", 0, "RAF_2_top_diffusenam", false};
    {"Topmat", 1, "nato_2_top_normal", true};
    {"Glovemat", 0, "nato_2_glove_diffuse", true};
    {"Glovemat", 1, "nato_2_glove_normal", true};
    {"Bottommat", 0, "RAF_2_bottom_diffusenam", false};
    {"Bottommat", 1, "nato_2_bottom_normal", true};
    {"Hatmat", 0, "tex_hat_diffuse", true};
    {"Hatmat", 1, "tex_hat_normal 1", true,};
   
}
name = "USAF-VIETNAM"


