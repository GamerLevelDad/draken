
livery = 
{
    
    {"Bodymat", 0, "tex_RAFWWIIbody_diffuse", false};
    {"Bodymat", 1, "tex_RAFWWIIbody_normal", false};
    {"Bottommat", 0, "Tex_Bottom_USAAF_diffuse", false};
    {"Bottommat", 1, "Tex_Bottom_USAAF_normal", false};
    {"Hatmat", 0, "Tex2_Hat_USAAF_diffuse", false};
    {"Hatmat", 1, "Tex2_Hat_USAAF_normal", false};
    {"Shoesmat", 0, "tex_RAFWWIIshoes_diffuse", false};
    {"Shoesmat", 1, "tex_RAFWWIIshoes_normal", false};
    {"Topmat", 0, "Tex_Top_diffuse_USAAF_use", false};
    {"Topmat", 1, "Tex_Top_USAAF_normal", false};
   
}
name = "USAAF WWII"


