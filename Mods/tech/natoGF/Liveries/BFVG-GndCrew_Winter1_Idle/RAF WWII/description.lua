
livery = 
{
    
    {"Bodymat", 0, "tex_RAFWWIIbody_diffuse", false};
    {"Bodymat", 1, "tex_RAFWWIIbody_normal", false};
    {"Bottommat", 0, "tex_RAFWWIIbottom_diffuse", false};
    {"Bottommat", 1, "tex_RAFWWIIbottom_normal", false};
    {"Hatmat", 0, "tex2_RAFWWIIhat_diffuse", false};
    {"Hatmat", 1, "tex2_RAFWWIIhat_normal", false};
    {"Shoesmat", 0, "tex_RAFWWIIshoes_diffuse", false};
    {"Shoesmat", 1, "tex_RAFWWIIshoes_normal", false};
    {"Topmat", 0, "tex_RAFWWIItop_diffuse", false};
    {"Topmat", 1, "tex_RAFWWIItop_normal", false};
   
}
name = "RAF WWII"


