
livery = 
{
    
    {"Bodymat", 0, "tex_RAFWWIIbody_diffuse", false};
    {"Bodymat", 1, "tex_RAFWWIIbody_normal", false};
    {"Bottommat", 0, "RAFM_Bottom_diffuse", false};
    {"Bottommat", 1, "RAFM_Bottom_normal", false};
    {"Hatmat", 0, "RAFM_Hat_diffuse", false};
    {"Hatmat", 1, "RAFM_Hat_normal", false};
    {"Shoesmat", 0, "RAFM_Shoes_diffuse", false};
    {"Shoesmat", 1, "RAFM_Shoes_normal", false};
    {"Topmat", 0, "RAFM_Top_diffuse", false};
    {"Topmat", 1, "RAFM_Top_normal", false};
   
}
name = "RAF MODERN"


