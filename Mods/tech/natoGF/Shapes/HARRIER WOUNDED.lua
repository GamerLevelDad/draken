-- rename it to description.lua
livery = 
{
    --[[
        uncomment lines for customized dds/tga/bmp files
    --]]
    --{"Object01SG", 0, "specter wounded body", true};
    --{"Object01SG", 2, "specter_global specular", true};
    --{"Object01SG1", 0, "specter wounded  eye", true};
    --{"Object01SG1", 2, "specter_global specular", true};
    --{"Object01SG2", 0, "specter wounded  head", true};
    --{"Object01SG2", 2, "specter_global specular", true};
    --{"Object01SG3", 0, "specter wounded  feet", true};
    --{"Object01SG3", 2, "specter_global specular", true};
    --{"Object01SG6", 0, "specter wounded navy", true};
    --{"Object01SG6", 2, "specter_global specular", true};
    --{"Object01SG5", 0, "specter wounded  lowdiff", true};
    --{"Object01SG5", 2, "specter_global specular", true};
    --{"Object01SG7", 0, "specter wounded  hand", true};
    --{"Object01SG7", 2, "specter_global specular", true};
    --{"Object01SG8", 0, "specter wounded  kit", true};
    --{"Object01SG8", 2, "specter_global specular", true};
    --{"02 - Default", 0, "specter wounded spk1 base_color", true};
    --{"02 - Default", 1, "specter wounded spk1 normal", true};
    --{"02 - Default", 2, "specter wounded spk1 specular", true};
}
----== below part is not required for cockpit livery ==----
--[[ name your own skin in default language (en)
     meanwhile, you can also name the skin in more than one languages,
     replace xx by [ru, cn, cs, de, es, fr, or it] ]]
name = ""
--name_xx = ""
--[[ assign the countries
     if you want no country limitation,
     then comment out below line]]
countries = {""}
