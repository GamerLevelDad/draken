--Infantry Ground Units & Ground Crew Mod - By SUNTSAG --

mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_texture_path  (current_mod_path.."/Textures/NATO.zip")
mount_vfs_texture_path  (current_mod_path.."/Textures/Crew.zip")

GT_t.CH_t.HUMAN = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -12.657,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}	
	
	
GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "soldier_ge_00.edm";
GT.visual.shape_dstr = "soldier_ge_00_d.edm";
GT.CustomAimPoint = {0,1.0,0};

GT.mobile = true;
GT.driverViewConnectorName = {"DRIVER_POINT", offset = {0.0, 3.89, 0.0}}
GT.sensor = {};
set_recursive_metatable(GT.sensor, GT_t.SN_visual);
GT.sensor.height = 1.8;
GT.sensor.max_range_finding_target = 500;

GT.WS = {};
GT.WS.maxTargetDetectionRange = 2000;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
GT.WS[ws].center = 'POINT_TOWER';
GT.WS[ws].angles = {
					{math.rad(45), math.rad(-45), math.rad(-15), math.rad(30)},
					};
GT.WS[ws].drawArgument1 = 0;
GT.WS[ws].drawArgument2 = 1;
GT.WS[ws].omegaY = math.rad(100);
GT.WS[ws].omegaZ = math.rad(100);
GT.WS[ws].pidY = {p=100,i=1.5,d=9,inn=10};
GT.WS[ws].pidZ = {p=100,i=1.5,d=9,inn=10,};
GT.WS[ws].stabilizer = true;
GT.WS[ws].laser = true;

__LN = add_launcher(GT.WS[ws], GT_t.LN_t.carabine_M4);
__LN.maxShootingSpeed = 0;
for i=2,8 do -- 8 clips, 240 rounds
	__LN.PL[i] = {};
	set_recursive_metatable(__LN.PL[i], __LN.PL[1]);
end
__LN.fireAnimationArgument = 23;
__LN.BR[1].pos = {1.0, 0.5, 0.1};
__LN.connectorFire = false;

GT.Name = "NATOSoldierOILTANKER";
GT.DisplayName = _("*NATO_Soldier_OILTANKER");
GT.Rate = 1;

GT.EPLRS = true

GT.DetectionRange  = 0;
GT.ThreatRange = GT.WS[1].LN[1].distanceMax;
GT.mapclasskey = "P0091000201";

GT.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,wsType_GenericIFV,
				"Infantry",
				"CustomAimPoint",
				"New infantry",
				"APC",
				"Datalink"
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)
	
GT_t.CH_t.HUMAN = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -12.657,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}	
	

GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "soldier_us_00.edm";
GT.visual.shape_dstr = "soldier_us_00_d.edm";
GT.CustomAimPoint = {0,1.0,0};

GT.mobile = true;
GT.driverViewConnectorName = {"DRIVER_POINT", offset = {0.0, 3.89, 0.0}}
GT.sensor = {};
set_recursive_metatable(GT.sensor, GT_t.SN_visual);
GT.sensor.height = 1.8;
GT.sensor.max_range_finding_target = 500;

GT.WS = {};
GT.WS.maxTargetDetectionRange = 2000;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
GT.WS[ws].center = 'POINT_TOWER';
GT.WS[ws].angles = {
					{math.rad(45), math.rad(-45), math.rad(-15), math.rad(30)},
					};
GT.WS[ws].drawArgument1 = 0;
GT.WS[ws].drawArgument2 = 1;
GT.WS[ws].omegaY = math.rad(100);
GT.WS[ws].omegaZ = math.rad(100);
GT.WS[ws].pidY = {p=100,i=1.5,d=9,inn=10};
GT.WS[ws].pidZ = {p=100,i=1.5,d=9,inn=10,};
GT.WS[ws].stabilizer = true;
GT.WS[ws].laser = true;

__LN = add_launcher(GT.WS[ws], GT_t.LN_t.carabine_M4);
__LN.maxShootingSpeed = 0;
for i=2,8 do -- 8 clips, 240 rounds
	__LN.PL[i] = {};
	set_recursive_metatable(__LN.PL[i], __LN.PL[1]);
end
__LN.fireAnimationArgument = 23;
__LN.BR[1].pos = {1.0, 0.5, 0.1};
__LN.connectorFire = false;

GT.Name = "USSoldierOILTANKER";
GT.DisplayName = _("*US_Soldier_OILTANKER");
GT.Rate = 1;

GT.EPLRS = true

GT.DetectionRange  = 0;
GT.ThreatRange = GT.WS[1].LN[1].distanceMax;
GT.mapclasskey = "P0091000201";

GT.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,wsType_GenericIFV,
				"Infantry",
				"CustomAimPoint",
				"New infantry",
				"APC",
				"Datalink"
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)	

GT_t.CH_t.HUMAN = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -12.657,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}	
GT = {};
set_recursive_metatable(GT, GT_t.generic_human)
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN);
GT.chassis.life = 30

GT.visual.shape = "HARRIER WOUNDED.EDM"
GT.visual.shape_dstr = "HARRIER WOUNDED_DEST.EDM"
GT.mobile = true; 

--Burning after hit
GT.visual.fire_size = 0.8 --relative burning size
GT.visual.fire_pos[1] = 0 -- center of burn at long axis shift(meters)
GT.visual.fire_pos[2] = 0 -- center of burn shift at vertical shift(meters)
GT.visual.fire_pos[3] = 0 -- center of burn at transverse axis shift(meters)
GT.visual.fire_time = 180 --burning time (seconds)	

GT.Name = "MedicsOILTANKER"
GT.DisplayName = _("*MEDICSOILTANKER")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000201";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,"Infantry","CustomAimPoint",};
GT.category = "Infantry";
GT.Transportable = {size = 300}
				
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1

