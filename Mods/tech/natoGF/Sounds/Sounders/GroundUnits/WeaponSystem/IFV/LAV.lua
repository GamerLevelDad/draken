dofile("Tools.lua")
dofile("GroundUnits/WeaponSystem/Tools/weapons.lua")

dofile("GroundUnits/WeaponSystem/Tools/AutoGunSounds.lua")

LAV25_weapons = weapons:new()

LAV25_weapons:addTurret(1)

LAV25_weapons:addLauncher(1, 1, automatic_gun_25mm)
LAV25_weapons:addLauncher(1, 2, machinegun_M240C)

LAV25_weapons:addTurret(2, "GndTech/SphericalHingeRotation")

LAV25_weapons:addLauncher(2, 1, machinegun_M240C)
