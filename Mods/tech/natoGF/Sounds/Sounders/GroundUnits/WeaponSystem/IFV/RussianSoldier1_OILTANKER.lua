dofile("Tools.lua")
dofile("GroundUnits/WeaponSystem/Tools/weapons.lua")

dofile("GroundUnits/WeaponSystem/Tools/AutoGunSounds.lua")

SoldierAK74_weapons = weapons:new()

SoldierAK74_weapons:addTurret(1)

SoldierAK74_weapons:addLauncher(1, 1, AK74)
