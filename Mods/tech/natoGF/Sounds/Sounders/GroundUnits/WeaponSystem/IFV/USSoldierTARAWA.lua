dofile("Tools.lua")
dofile("GroundUnits/WeaponSystem/Tools/weapons.lua")

dofile("GroundUnits/WeaponSystem/Tools/AutoGunSounds.lua")

SoldierM4_weapons = weapons:new()

SoldierM4_weapons:addTurret(1)
SoldierM4_weapons:addLauncher(1, 1, carabine_M4)
