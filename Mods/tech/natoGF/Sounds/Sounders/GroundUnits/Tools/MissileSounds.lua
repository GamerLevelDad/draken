Fagot = {
    single_shot = "Weapons/MissileLaunch2",
}

Reflex = {
    single_shot = "Weapons/MissileLaunch2",
	start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

_9M120 = {
    single_shot = "Weapons/MissileLaunch2",
}

_9A33 = {
    single_shot = "Weapons/MissileLaunch3",
}

_9A330 = {
    single_shot = "Weapons/MissileLaunch2",
}

HARPOON = {
    single_shot = "Weapons/MissileLaunch2",
}

M192 = {
    single_shot = "Weapons/MissileLaunch2",
}

_9P31 = {
    single_shot = "Weapons/MissileLaunch3",
	sound_locking_target = "Aircrafts/Cockpits/SidewinderLow",
	sound_locked_target = "Aircrafts/Whistle"
}

M901 = {
    single_shot = "Weapons/MissileLaunch2",
}

igla = {
    single_shot = "Weapons/MissileLaunch3",
	sound_locking_target = "Aircrafts/Cockpits/SidewinderLow",
	sound_locked_target = "Aircrafts/Whistle"
}

roland = {
    single_shot = "Weapons/MissileLaunch2",
}

_9M311 = {
    single_shot = "Weapons/MissileLaunch2",
}

_2P25 = {
    single_shot = "Weapons/MissileLaunch2",
}

M48 = {
    single_shot = "Weapons/MissileLaunch2",
	sound_locking_target = "Aircrafts/Cockpits/SidewinderLow",
	sound_locked_target = "Aircrafts/Cockpits/SidewinderHigh"
}

stinger = {
    single_shot = "Weapons/MissileLaunch3",
	sound_locking_target = "Aircrafts/Cockpits/SidewinderLow",
	sound_locked_target = "Aircrafts/Cockpits/SidewinderHigh"
}

Kornet = {
    single_shot = "Weapons/MissileLaunch2",
}

_9A310M1 = {
    single_shot = "Weapons/MissileLaunch2",
}

S125 = {
    single_shot = "Weapons/MissileLaunch2",
}

Svir = {
    single_shot = "Weapons/MissileLaunch2",
	start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

Malutka = {
    single_shot = "Weapons/MissileLaunch2",
}

_9M117 = {
    single_shot = "Weapons/MissileLaunch2",
	start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

_9A35 = {
    single_shot = "Weapons/MissileLaunch2",
	sound_locking_target = "Aircrafts/Cockpits/SidewinderLow",
	sound_locked_target = "Aircrafts/Cockpits/SidewinderHigh"
}

TOW = {
    single_shot = "Weapons/TowLaunch",
}

igla_comm_manpad = {
    single_shot = "Weapons/MissileLaunch3",
}

stinger_comm_manpad = {
    single_shot = "Weapons/MissileLaunch3",
}

stinger_manpad = {
    single_shot = "Weapons/MissileLaunch3",
	sound_locking_target = "Aircrafts/Cockpits/SidewinderLow",
	sound_locked_target = "Aircrafts/Cockpits/SidewinderHigh"
}

ship_MK41_SM2 = {
    single_shot = "Weapons/MissileLaunch2",
}

seasparrow = {
    single_shot = "Weapons/MissileLaunch2",
}

ship_TOMAHAWK = {
    single_shot = "Weapons/MissileLaunch2",
}

ship_rif = {
    single_shot = "Weapons/MissileLaunch2",
}

ship_MOSKIT = {
    single_shot = "Weapons/MissileLaunch2",
}

ship_bazalt = {
    single_shot = "Weapons/MissileLaunch2",
}

ship_klinok = {
    single_shot = "Weapons/MissileLaunch2",
}

ship_granit = {
    single_shot = "Weapons/MissileLaunch2",
}

ship_rifM = {
    single_shot = "Weapons/MissileLaunch2",
}

GRAD_9M22U = {
	single_shot = "Weapons/GradFiring",
}

URAGAN_9M27F = {
	single_shot = "Weapons/MissileLaunch4",
}

SMERCH_9M55K = {
	single_shot = "Weapons/MissileLaunch4",
}

M26 = {
	single_shot = "Weapons/MissileLaunch4",
}