howitzer_M185 = {
    single_shot = "Weapons/Cannon/125mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

tank_gun_105mm = {
    single_shot = "Weapons/Cannon/105mmGun",
    single_shot_hall = "Weapons/Cannon/105mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

tank_gun_CN_120_26 = {
    single_shot = "Weapons/Cannon/120mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

howitzer_2A60 = {
    single_shot = "Weapons/Cannon/120mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

RPG = {
    single_shot = "Weapons/Cannon/Human_Grenade_Launcher",
}

gun_2A70 = {
    single_shot = "Weapons/Cannon/105mmGun",
    single_shot_hall = "Weapons/Cannon/105mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

howitzer_2A18 = {
    single_shot = "Weapons/Cannon/125mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

tank_gun_2A28 = {
    single_shot = "Weapons/Cannon/73mmGun",
    single_shot_hall = "Weapons/Cannon/73mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

tank_gun_120mm = {
    single_shot = "Weapons/Cannon/120mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

howitzer_2A64 = {
    single_shot = "Weapons/Cannon/125mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

howitzer_2A33 = {
    single_shot = "Weapons/Cannon/125mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

howitzer_DANA = {
    single_shot = "Weapons/Cannon/125mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

tank_gun_2A46 = {
    single_shot = "Weapons/Cannon/125mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
    start_reload = "Weapons/Cannon/Reload1",
    end_reload = "Weapons/Cannon/Reload2",
}

AK130 = {
    single_shot ="Weapons/Cannon/73mmGun",
    single_shot_hall = "Weapons/Cannon/73mmGun_hall",
}

ship_MK75 = {
    single_shot ="Weapons/Cannon/73mmGun",
    single_shot_hall = "Weapons/Cannon/73mmGun_hall",
}

AK100 = {
    single_shot ="Weapons/Cannon/73mmGun",
    single_shot_hall = "Weapons/Cannon/73mmGun_hall",
}

ship_FMC5 = {
    single_shot ="Weapons/Cannon/125mmGun",
    single_shot_hall = "Weapons/Cannon/125mmGun_hall",
}

ship_AK176 = {
    single_shot ="Weapons/Cannon/73mmGun",
    single_shot_hall = "Weapons/Cannon/73mmGun_hall",
}