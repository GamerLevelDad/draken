AK74 = {
    cycle_shot = "Weapons/Automatic/AK-74",
    end_burst = "Weapons/Automatic/AK-74_End",
}

machinegun_7_62 = {
    cycle_shot = "Weapons/Automatic/PKT",
    end_burst = "Weapons/Automatic/PKT_End",
}

carabine_M4 = {
    cycle_shot = "Weapons/Automatic/MachineGunFire_5_56",
    end_burst = "Weapons/Automatic/MachineGunFire_5_56_End",
}

automatic_gun_2A72 = {
    cycle_shot = "Weapons/Automatic/2A72",
    end_burst = "Weapons/Automatic/2A72_End",
}

GSH_6_30K = {
    cycle_shot = "Weapons/Automatic/GSh301",
    end_burst = "Weapons/Automatic/GSh301End",
}

automatic_gun_25mm = {
    single_shot = "Weapons/Automatic/M242Single",
}

MK19 = {
    cycle_shot = "Weapons/Automatic/MK_19",
    end_burst = "Weapons/Automatic/MK_19_End",
}

AG17 = {
    cycle_shot = "Weapons/Automatic/AGS-17",
    end_burst = "Weapons/Automatic/AGS-17_End",
}

automatic_gun_2A42 = {
    cycle_shot = "Weapons/Automatic/2A42",
	end_burst = "Weapons/Automatic/2A42_End",
}

machinegun_M249 = {
    cycle_shot = "Weapons/Automatic/M249",
    end_burst = "Weapons/Automatic/M249_End",
}

automatic_gun_2A14 = {
    cycle_shot = "Weapons/Automatic/ZU_23_2",
    end_burst = "Weapons/Automatic/ZU_23_2_End",
}

automatic_gun_2A14_2 = {
    cycle_shot = "Weapons/Automatic/ZU_23_4",
    end_burst = "Weapons/Automatic/ZU_23_4_End",
}

L94A1 = {
    cycle_shot = "Weapons/Automatic/MachineGunFire_520rpm",
    end_burst = "Weapons/Automatic/MachineGunFire_520rpm_End",
}

automatic_gun_RH202 = {
    cycle_shot = "Weapons/Automatic/Rheinmetall_Mk20",
    end_burst = "Weapons/Automatic/Rheinmetall_Mk20_End",
}

automatic_gun_KPVT = {
    cycle_shot = "Weapons/Automatic/KPVT",
    end_burst = "Weapons/Automatic/KPVT_End",
}

automatic_gun_2A38 = {
    cycle_shot = "Weapons/Automatic/Tunguska",
    end_burst = "Weapons/Automatic/Tunguska_End",
}

machinegun_12_7_M2 = {
    cycle_shot = "Weapons/Automatic/M2_Browning",
    end_burst = "Weapons/Automatic/M2_Browning_End",
}

automatic_gun_KDA = {
    cycle_shot = "Weapons/Automatic/gepard_KDA",
    end_burst = "Weapons/Automatic/gepard_KDA_End",
}

machinegun_M240C = {
    cycle_shot = "Weapons/Automatic/M249",
    end_burst = "Weapons/Automatic/M249_End",
}

PKT = {
    cycle_shot = "Weapons/Automatic/PKT",
    end_burst = "Weapons/Automatic/PKT_End",
}

machinegun_MG3 = {
    cycle_shot = "Weapons/Automatic/MG3",
    end_burst = "Weapons/Automatic/MG3_End",
}

automatic_gun_M168 = {
    cycle_shot = "Weapons/Automatic/M163_Vulcan",
    end_burst = "Weapons/Automatic/M163_Vulcan_End",
}

automatic_gun_L21A1 = {
    single_shot = "Weapons/Automatic/Warrior",
}

machinegun_12_7_utes = {
    cycle_shot = "Weapons/Automatic/Utes",
    end_burst = "Weapons/Automatic/Utes_End",
}

AK630 = {
    cycle_shot = "Weapons/Automatic/GSh301",
    end_burst = "Weapons/Automatic/GSh301End",
}

phalanx = {
    cycle_shot = "Weapons/Automatic/Phalanx",
    end_burst = "Weapons/Automatic/Phalanx_End",
}