dofile("Tools.lua")
dofile("GroundUnits/WeaponSystem/Tools/Turret.lua")
dofile("GroundUnits/WeaponSystem/Tools/Launcher.lua")
host = ED_AudioAPI.createHost(ED_AudioAPI.ContextWorld, "main")
weapons = {cockpit = {}, turret_list = {}, launchers_list = {}}
--weapons.cockpit.initialized = false
--weapons.cockpit.hostPosition = {-0.7, -0.5, 0}

function weapons:new()
	o = {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function weapons:addTurret(ws_id, drive_sound_filename)
	if ws_id == nil or ws_id < 1 then
		return
	end

	local turret_ = class_turret:new()

	turret_:init(host, drive_sound_filename)
	turret_.ID = ws_id
	table.insert(self.turret_list, ws_id, turret_)
end

function weapons:updateTurret(ws_id, rotationSpeed)
	if self.turret_list[ws_id] ~= nil then
		self.turret_list[ws_id]:update(rotationSpeed)
	end
end
function onUpdate(params)
	updateHost(host, params)
	weapons:updateTurret(params.WsId, params.RotationSpeedY)
end

function weapons:stopTurret(ws_id)
	if self.turret_list[ws_id] ~= nil then
		self.turret_list[ws_id]:stop_play()
	end
end
function onEvent_StopTurret(arg1)
	weapons:stopTurret(arg1)
end

function weapons:addLauncher(ws_id, ln_id, launcher_template)
	local launcher_ = class_launcher:new(launcher_template, host)
	launcher_.ID = ln_id
	self.launchers_list = self.launchers_list or {}
	if ws_id > #(self.launchers_list) then 
		table.insert(self.launchers_list, ws_id, {})
	else
		self.launchers_list[ws_id] = self.launchers_list[ws_id] or {}
	end
	table.insert(self.launchers_list[ws_id], ln_id, launcher_)
end

function weapons:onInit(params) -- stub
	
end

function weapons:singleShot(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:single_shot()
	end
end
function onEvent_SingleShot(arg1, arg2)
	weapons:singleShot(arg1, arg2)
end

function weapons:startBurst(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:start_burst()
	end
end
function onEvent_StartBurst(arg1, arg2)
	weapons:startBurst(arg1, arg2)
end

function weapons:endBurst(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:end_burst()
	end
end
function onEvent_EndBurst(arg1, arg2)
	weapons:endBurst(arg1, arg2)
end


function weapons:reloadFirst(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:reload_first()
	end
end
function onEvent_ReloadFirst(arg1, arg2)
	weapons:reloadFirst(arg1, arg2)
end

function weapons:reloadSecond(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:reload_second()
	end
end
function onEvent_ReloadSecond(arg1, arg2)
	weapons:reloadSecond(arg1, arg2)
end
function onEvent_CreateTurret(arg1, arg2) -- stub
	
end
function onEvent_CreateLauncher(arg1, arg2, arg3) -- stub
	
end

function weapons:IrQuiet(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:IrQuiet()
	end
end
function onEvent_IrQuiet(arg1, arg2)
	weapons:IrQuiet(arg1, arg2)
end

function weapons:IrLocking(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:IrLocking()
	end
end
function onEvent_IrLocking(arg1, arg2)
	weapons:IrLocking(arg1, arg2)
end

function weapons:IrLocked(arg1, arg2)
	if self.launchers_list[arg1] and self.launchers_list[arg1][arg2] then
		self.launchers_list[arg1][arg2]:IrLocked()
	end
end
function onEvent_IrLocked(arg1, arg2)
	weapons:IrLocked(arg1, arg2)
end

--[[
function weapons:createEngineCpt()
	self.engine:createSoundsCpt(self.cockpit.host)
end

function weapons:destroyEnginesCpt()
	self.engine:destroySoundsCpt()
end

function weapons:createCockpit()
	self.cockpit.host           = ED_AudioAPI.createHost(ED_AudioAPI.ContextCockpit, "cockpitMain")
	ED_AudioAPI.setHostPosition(self.cockpit.host, self.cockpit.hostPosition[1], self.cockpit.hostPosition[2], self.cockpit.hostPosition[3])
	self.cockpit.hostHeadphones = ED_AudioAPI.createHost(ED_AudioAPI.ContextHeadphones, "cockpitHeadphones")
	--self.cockpit.hostCpt2D      = ED_AudioAPI.createHost(ED_AudioAPI.ContextCockpit2D, "cockpit2D")
	
	self.cockpit.initialized = true
	
	self:createEngineCpt()
end

function weapons:releaseCockpit()
	self:destroyEnginesCpt()
	
	ED_AudioAPI.destroyHost(self.cockpit.host)
	ED_AudioAPI.destroyHost(self.cockpit.hostHeadphones)
	--ED_AudioAPI.destroyHost(self.cockpit.hostCpt2D)

	self.cockpit.initialized = false
end

function onEvent_cockpitCreate()
	weapons:createCockpit()
end

function onEvent_cockpitDestroy()
	weapons:releaseCockpit()
end
]]
