class_turret = {drive_sound = "GndTech/TurretRotation"}
-- self.drive_sound value can be changed in child to redefine rotation sound

function class_turret:new()
	local object = {}
	
	setmetatable(object, self)
	self.__index = self
	
	return object
end

function class_turret:init(host, drive_sound_filename)
	if drive_sound_filename == "empty" then
		return
	elseif drive_sound_filename ~= "" then
		self.drive_sound = drive_sound_filename
	end
	self.sndDrive = ED_AudioAPI.createSource(host, self.drive_sound)
		
end

function class_turret:calc_pitch_gain(rotationSpeedY)
	local rotSpdY = math.abs(rotationSpeedY)
	local pitch = (rotSpdY < 0.2) and (0.55 * rotSpdY + 0.65) or math.min(1.0, 0.3 * rotSpdY + 0.7)
	local gain = (rotSpdY < 0.3) and (2.4 * rotSpdY) or math.min(1.0, 0.4 * rotSpdY + 0.6)
	
	return pitch, gain
end

function class_turret:stop_play()
	if self.sndDrive ~= nil then
		ED_AudioAPI.stopSource(self.sndDrive)
	end
end

function class_turret:update(RotationSpeedY)
	local pitch, gain = self:calc_pitch_gain(RotationSpeedY)
	
	if self.sndDrive ~= nil then
		ED_AudioAPI.setSourcePitch(self.sndDrive, pitch)
		ED_AudioAPI.setSourceGain(self.sndDrive, gain)
	
		ED_AudioAPI.playSourceLooped(self.sndDrive)
	end
end
