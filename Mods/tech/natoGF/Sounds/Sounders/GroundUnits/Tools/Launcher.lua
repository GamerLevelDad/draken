class_launcher = {}

function class_launcher:new(launcher_template, sound_host)
	local object = {}
	
	if launcher_template.single_shot ~= nil then
		object.single_shot_source = ED_AudioAPI.createSource(sound_host, launcher_template.single_shot)
	end
	
	if launcher_template.single_shot_hall ~= nil then
		object.single_shot_hall_source = ED_AudioAPI.createSource(sound_host, launcher_template.single_shot_hall)
	end
	
	if launcher_template.cycle_shot ~= nil then
		object.cycle_shot_source = ED_AudioAPI.createSource(sound_host, launcher_template.cycle_shot)
	end
	
	if launcher_template.end_burst ~= nil then
		object.end_burst_source = ED_AudioAPI.createSource(sound_host, launcher_template.end_burst)
	end
	
	if launcher_template.start_reload ~= nil then
		object.start_reload_source = ED_AudioAPI.createSource(sound_host, launcher_template.start_reload)
	end
	
	if launcher_template.end_reload ~= nil then
		object.end_reload_source = ED_AudioAPI.createSource(sound_host, launcher_template.end_reload)
	end
	
	if launcher_template.sound_locking_target ~= nil then
		local ir_missile_host = ED_AudioAPI.createHost(ED_AudioAPI.ContextHeadphones, "IR_MISSILE_HEADPONES")
		object.sound_locking_target_source = ED_AudioAPI.createSource(ir_missile_host, launcher_template.sound_locking_target)
	end
	
	if launcher_template.sound_locked_target ~= nil then
		local ir_missile_host = ED_AudioAPI.createHost(ED_AudioAPI.ContextHeadphones, "IR_MISSILE_HEADPONES")
		object.sound_locked_target_source = ED_AudioAPI.createSource(ir_missile_host, launcher_template.sound_locked_target)
	end

	setmetatable(object, self)
	self.__index = self
	
	return object
end

function class_launcher:single_shot()
	if self.single_shot_source ~= nil then
		ED_AudioAPI.playSourceOnce(self.single_shot_source)
	end
	if self.single_shot_hall_source ~= nil then
		ED_AudioAPI.playSourceOnce(self.single_shot_hall_source)
	end
end

function class_launcher:start_burst()
	if self.end_burst_source ~= nil then
		ED_AudioAPI.stopSource(self.end_burst_source)
	end
	
	if self.cycle_shot_source ~= nil then
		ED_AudioAPI.playSourceLooped(self.cycle_shot_source)
	end
end

function class_launcher:end_burst()
	if self.cycle_shot_source ~= nil then
		ED_AudioAPI.stopSource(self.cycle_shot_source)
	end
	
	if self.end_burst_source ~= nil then
		ED_AudioAPI.playSourceOnce(self.end_burst_source)
	end
end

function class_launcher:reload_first()
	if self.start_reload_source ~= nil then
		ED_AudioAPI.playSourceOnce(self.start_reload_source)
	end
end

function class_launcher:reload_second()
	if self.end_reload_source ~= nil then
		ED_AudioAPI.playSourceOnce(self.end_reload_source)
	end
end

function class_launcher:IrQuiet()
	if self.sound_locking_target_source ~= nil then
		ED_AudioAPI.stopSource(self.sound_locking_target_source)
	end
	if self.sound_locked_target_source ~= nil then
		ED_AudioAPI.stopSource(self.sound_locked_target_source)
	end
end

function class_launcher:IrLocking()
	if self.sound_locking_target_source ~= nil then
		ED_AudioAPI.playSourceLooped(self.sound_locking_target_source)
	end
	if self.sound_locked_target_source ~= nil then
		ED_AudioAPI.stopSource(self.sound_locked_target_source)
	end
end

function class_launcher:IrLocked()
	if self.sound_locked_target_source ~= nil then
		ED_AudioAPI.playSourceLooped(self.sound_locked_target_source)
	end
	if self.sound_locking_target_source ~= nil then
		ED_AudioAPI.stopSource(self.sound_locking_target_source)
	end
end
