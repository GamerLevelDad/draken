declare_plugin("Air Power Unit by Tango",
{
installed 	 	= true, 
dirName	  	 	= current_mod_path,
displayName   	= _("Air Power Unit"),
shortName 		= "APU",
version		 	= __DCS_VERSION__,		 
state		 	= "installed",
developerName	= "Virtual Patrouille Suisse",
info		 	= _("Swiss Air Force Air Power Unit"),

}
)
---------------------------------------------------------------------------------------
dofile(current_mod_path.."/Air Power Unit.lua")
plugin_done()
