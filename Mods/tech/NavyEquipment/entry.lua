declare_plugin("Navy Equipment",
{
installed 	 = true,
dirName	  	 = current_mod_path,
developerName = "VSN ModTeam",
version		 = "2.5.6",		 
state		 = "installed",
info		 = _("Some Navy Equipment. Holdbackbars, Towbars, Towtrucks, Forklifts"),
Skins	=
	{
		{
			name	= _("Navy Equipment"),
			dir		= "Theme"
		},
	},
})
---------------------------------------------------------------------------------------
mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_texture_path  (current_mod_path.."/Textures/TOW")
mount_vfs_texture_path  (current_mod_path.."/Textures/IFLOLS_Mk14")
mount_vfs_liveries_path (current_mod_path.."/Liveries/")

dofile(current_mod_path..'/GTR18.lua')

dofile(current_mod_path.."/Database/db_NEquip_objects.lua")

plugin_done()