-- Tow Klein
CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "cvn_a-s32a-31a"
	CWAY.visual.shape_dstr = "cvn_a-s32a-31a"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "cvn_a-s32a-31a"
	CWAY.DisplayName = _("US Navy - MD-3 Tow Tractor empty")
	CWAY.Rate = 1
	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- Tow Klein+towbar
CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "cvn_a-s32a-31a_towbar"
	CWAY.visual.shape_dstr = "cvn_a-s32a-31a_towbar"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "cvn_a-s32a-31a_towbar"
	CWAY.DisplayName = _("US Navy - MD-3 Tow Tractor with Towbar empty")
	CWAY.Rate = 1
	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)

-- Tow Groß
CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "cvn_msu-200nav"
	CWAY.visual.shape_dstr = "cvn_msu-200nav"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "cvn_msu-200nav"
	CWAY.DisplayName = _("US Navy - MD-3 Air Tractor empty")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- Tow Fire
CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "cvn_a-s32a-31a-fire"
	CWAY.visual.shape_dstr = "cvn_a-s32a-31a-fire"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "cvn_a-s32a-31a-fire"
	CWAY.DisplayName = _("US Navy - MD-3 Fire Tractor empty")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	
-- Gabelstabler
CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "cvn_gabelstapler"
	CWAY.visual.shape_dstr = "cvn_gabelstapler"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        CWAY.animation_arguments.headlights = 1;	
	
        CWAY.Name = "cvn_gabelstapler"
	CWAY.DisplayName = _("US Navy - Forklift empty")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000005";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)
	

