local function add_structure(f)
	if(f) then
		f.shape_table_data = 
		{
			{
				file  	    = f.ShapeName,
				life		= f.Life,
				username    = f.Name,
				desrt       = f.ShapeNameDestr or "self",
			    classname 	= f.classname   or "lLandVehicle",
				positioning = f.positioning or "ADD_HEIGTH" -- {"BYNORMAL", "ONLYHEIGTH", "BY_XZ", "ADD_HEIGTH"}
			}
		}
		if f.ShapeNameDestr then
			f.shape_table_data[#f.shape_table_data + 1] = 
			{
				name  = f.ShapeNameDestr,
				file  = f.ShapeNameDestr,	
			}
		end
		
		
		f.mapclasskey = "P0091000076";
		f.attribute = {wsType_Static, wsType_Standing}
		
		add_surface_unit(f)
		GT = nil;
	else
		error("Can't add structure")
	end;
end
---------------- Buildings -----------
add_structure({
Name 		 =  "FCLP Box lights",                -- Landingstrip light
DisplayName  = _("US Navy - FCLP Box lights"),
ShapeName	 =   "cvn_Landebahn",
ShapeNameDestr = "cvn_Landebahn",
Life		 =  1000,
Rate		 =  1,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
---------------- Vehicles ------------
add_structure({
Name 		 =  "cvn_a-s32a-31a",                -- Tow Klein
DisplayName  = _("US Navy - MD-3 Tow Tractor empty"),
ShapeName	 =   "cvn_a-s32a-31a",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_a-s32a-31a_towbar",                -- Tow Klein+towbar
DisplayName  = _("US Navy - MD-3 Tow Tractor with Towbar empty"),
ShapeName	 =   "cvn_a-s32a-31a_towbar",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_msu-200nav",                -- Tow Air Tractor
DisplayName  = _("US Navy - MD-3 Air Tractor empty"),
ShapeName	 =   "cvn_msu-200nav",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_a-s32a-31a-fire",                -- Tow Fire
DisplayName  = _("US Navy - MD-3 Fire Tractor empty"),
ShapeName	 =   "cvn_a-s32a-31a-fire",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_gabelstapler",                -- Gabelstapler
DisplayName  = _("US Navy - Forklift empty"),
ShapeName	 =   "cvn_gabelstapler",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "IFLOLS_Mk14",                -- IFLOLS_Mk14
DisplayName  = _("US Navy - IFLOLS Mk14"),
ShapeName	 =   "IFLOLS_Mk14",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "IFLOLS_Mk14 Meatball",                -- IFLOLS_Mk14 with Anubis Meatball
DisplayName  = _("US Navy - IFLOLS Mk14 working"),
ShapeName	 =   "IFLOLS_Mk14_Meatball",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_baggage_truck",                -- Baggage Truck
DisplayName  = _("US Navy - Baggage Truck"),
ShapeName	 =   "cvn_baggage_truck",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_epu_truck",                -- NAS EPU Truck
DisplayName  = _("US Navy - NAS EPU Truck empty"),
ShapeName	 =   "cvn_epu_truck",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_service-truck",                -- NAS Service Truck
DisplayName  = _("US Navy - NAS Service Truck empty"),
ShapeName	 =   "cvn_service-truck",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_utility_cart",                -- Utility Cart
DisplayName  = _("US Navy - Utility Cart"),
ShapeName	 =   "cvn_utility_cart",
ShapeNameDestr = "oblomok-4",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
---------------- Objects ------------
add_structure({
Name 		 =  "towbar_offen",                -- Towbar open
DisplayName  = _("US Navy - Towbar open"),
ShapeName	 =   "cvn_towbar_offen",
ShapeNameDestr = "cvn_towbar_offen",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "towbar_zu",                -- Towbar closed
DisplayName  = _("US Navy - Towbar closed"),
ShapeName	 =   "cvn_towbar_zu",
ShapeNameDestr = "cvn_towbar_zu",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_holdbackbar",                -- Holdback Bar
DisplayName  = _("US Navy - Holdback Bar"),
ShapeName	 =   "cvn_holdbackbar",
ShapeNameDestr = "cvn_holdbackbar",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "radkeil",                -- Wheel Chock
DisplayName  = _("US Navy - Wheel Chock"),
ShapeName	 =   "cvn_radkeil",
ShapeNameDestr = "cvn_radkeil",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "cvn_Feuerloescher",                -- Fire Extinguisher
DisplayName  = _("US Navy - Fire Extinguisher"),
ShapeName	 =   "cvn_fire_extinguisher",
ShapeNameDestr = "cvn_fire_extinguisher",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})
add_structure({
Name 		 =  "gtr18_base_projectile",                -- GTR 18 Smokey Sam
DisplayName  = _("GTR-18 Smokey Sam - static"),
ShapeName	 =   "gtr18_base_projectile",
ShapeNameDestr = "gtr18_base_projectile",
Life		 =  10,
Rate		 =  10,
category     =  'Fortification',
SeaObject    = 	false,
isPutToWater =  false,
})