declare_plugin("476 vFG Range Targets by Noodle & Stuka",
{

installed 	 = true, 
dirName	  	 = current_mod_path,
version		 = "16 JUN 2019",		 
state		 = "installed",


shortName = '476th - Range Targets',
fileMenuName = "476th - Range Targets",
info		  = _("This add-on includes new targets and supporting structures which are appropriate for use on both conventional and tactical training ranges."),
Skins	=
	{
		{
			name	= "Range Targets",
			dir		= "Theme"
		},
	},

}
)
---------------------------------------------------------------------------------------
mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_texture_path (current_mod_path.."/Textures/Range_Target.zip")
mount_vfs_texture_path (current_mod_path.."/Textures")

dofile(current_mod_path..'/Range Objects.lua')
dofile(current_mod_path..'/Containers.lua')
dofile(current_mod_path..'/NTTR Targets.lua')
dofile(current_mod_path..'/Airfield Objects.lua')


plugin_done()
