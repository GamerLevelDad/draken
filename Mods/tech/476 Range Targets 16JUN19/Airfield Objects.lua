GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "fclp_box"
GT.visual.shape_dstr = "fclp_box"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "fclp_box"
GT.DisplayName = _("*FCLP Box")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)