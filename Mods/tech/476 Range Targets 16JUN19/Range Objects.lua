GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Aim_Off_Marker"
GT.visual.shape_dstr = "Aim_Off_Marker"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Aim_Off_Marker"
GT.DisplayName = _("*Aim-Off Distance Marker")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Conventional_Circle_A"
GT.visual.shape_dstr = "Conventional_Circle_A"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Conventional_Circle_A"
GT.DisplayName = _("*Conventional Circle A")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Conventional_Circle_B"
GT.visual.shape_dstr = "Conventional_Circle_B"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Conventional_Circle_B"
GT.DisplayName = _("*Conventional Circle B")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Foul_Line"
GT.visual.shape_dstr = "Foul_Line"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Foul_Line"
GT.DisplayName = _("*Foul Line")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Range_Tower_Main"
GT.visual.shape_dstr = "Range_Tower_Main"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;
GT.driverViewPoint = {3.7, 17.0, 0.0};

GT.Name = "Range_Tower_Main"
GT.DisplayName = _("*Range Tower (Main)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Strafe_Pit"
GT.visual.shape_dstr = "Strafe_Pit"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Strafe_Pit"
GT.DisplayName = _("*Strafe Pit")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Tac_Bomb_MiG-17"
GT.visual.shape_dstr = "Tac_Bomb_MiG-17"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Tac_Bomb_MiG-17"
GT.DisplayName = _("*Silhouette (MiG-17)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Tac_Bomb_MiG-29"
GT.visual.shape_dstr = "Tac_Bomb_MiG-29"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Tac_Bomb_MiG-29"
GT.DisplayName = _("*Silhouette (MiG-29)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);

GT.visual.shape = "Tac_Bunker_Sand_1"
GT.visual.shape_dstr = "tac_bunker_sand_1_destr"
GT.chassis.life = 500; 
GT.visual.fire_size = 0.9 
GT.visual.fire_pos[1] = -1 
GT.visual.fire_pos[2] = 0 
GT.visual.fire_pos[3] = 0 
GT.visual.fire_time = 900 

GT.Name = "Tac_Bunker_Sand_1"
GT.DisplayName = _("*Bunker, Concrete (Sand 1)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Tac_Revet_50x50x6_Sand_1"
GT.visual.shape_dstr = "Tac_Revet_50x50x6_Sand_1"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Tac_Revet_50x50x6_Sand_1"
GT.DisplayName = _("*Revet 50x50x6 (Sand 1)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Tac_Strafe_Hard"
GT.visual.shape_dstr = "Tac_Strafe_Hard"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Tac_Strafe_Hard"
GT.DisplayName = _("*Tac Strafe (Hard)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Tac_Strafe_Soft"
GT.visual.shape_dstr = "Tac_Strafe_Soft"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Tac_Strafe_Soft"
GT.DisplayName = _("*Tac Strafe (Soft)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
