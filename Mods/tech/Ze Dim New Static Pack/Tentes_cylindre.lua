mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_texture_path  (current_mod_path.."/Textures/tente_cylindre")









--tentepetite_vert
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 1

	CWAY.visual.shape = "tentepetite_vert"
	CWAY.visual.shape_dstr = "tentepetite_destr.edm"


	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 1;
        
	
        CWAY.Name = "tentepetite_vert"
	CWAY.DisplayName = _("FARP CYLINDER TENT GREEN")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)



--tentepetite_desert
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_stationary)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.STATIC);
	CWAY.chassis.life = 1

	CWAY.visual.shape = "tentepetite_desert"
	CWAY.visual.shape_dstr = "tentepetite_destr.edm"


	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 1;
        
	
        CWAY.Name = "tentepetite_desert"
	CWAY.DisplayName = _("FARP CYLINDER TENT DESERT")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000076";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsTypeVAZ,
					"Fortifications",
					};
	CWAY.category = "Fortification";

	add_surface_unit(CWAY)


