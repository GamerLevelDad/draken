local self_ID  = "Draken International Mercenaries by Shrike"
declare_plugin(self_ID,
{

installed 	 = true, 
dirName	  	 = current_mod_path,
version		 = "1.0",		 
state		 = "installed",


shortName = 'Draken Mercenaries',
fileMenuName = _"Draken Mercenaries",
info		  = _("This Addon Creates the Mercenary Group Draken International, and all of the supporting Missions and Campaigns."),
Skins	=
	{
		{
			name	= _"Draken Mercenaries",
			dir		= "Skins/1"
		},
	},
	
	
Missions =
	{
		{
			name		= _("Draken Intl Mercenaries"),
			dir			= "Missions",
			CLSID		= "",
		},
	},

}
)

mount_vfs_texture_path	(current_mod_path .."/Skins/1/ME" )	--for simulator loading window
---------------------------------------------------------------------------------------
--mount_vfs_model_path	(current_mod_path.."/Shapes")
--mount_vfs_texture_path (current_mod_path.."/Textures/Range_Target.zip")
--mount_vfs_texture_path (current_mod_path.."/Textures")

--dofile(current_mod_path..'/Range Objects.lua')
--dofile(current_mod_path..'/Containers.lua')
--dofile(current_mod_path..'/NTTR Targets.lua')
--dofile(current_mod_path..'/Airfield Objects.lua')


plugin_done()





