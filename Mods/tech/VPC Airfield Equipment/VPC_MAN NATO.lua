mount_vfs_model_path	(current_mod_path.."/Shapes NATO")
mount_vfs_liveries_path (current_mod_path.."/Liveries")
mount_vfs_texture_path  (current_mod_path.."/Textures NATO/VPC_NATO_textures.zip")

	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_human)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.HUMAN);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-MAN 1.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-MAN 1.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VPC-NATO-MAN 1"
	CWAY.DisplayName = _("VPC-NATO-MAN 1")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000201";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,
                "Infantry",
					};
					
	CWAY.category = "Infantry";
	
	add_surface_unit(CWAY)	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_human)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.HUMAN);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-MAN 2.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-MAN 2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VPC-NATO-MAN 2"
	CWAY.DisplayName = _("VPC-NATO-MAN 2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000201";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,
                "Infantry",
					};
					
	CWAY.category = "Infantry";
	
	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_human)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.HUMAN);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-MAN 3.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-MAN 3.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VPC-NATO-MAN 3"
	CWAY.DisplayName = _("VPC-NATO-MAN 3")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000201";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,
                "Infantry",
					};
					
	CWAY.category = "Infantry";
	
	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_human)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.HUMAN);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-MAN 3 v2.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-MAN 3 v2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VPC-NATO-MAN 3 v2"
	CWAY.DisplayName = _("VPC-NATO-MAN 3 v2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000201";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,
                "Infantry",
					};
					
	CWAY.category = "Infantry";
	
	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_human)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.HUMAN);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-MAN 4.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-MAN 4.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VPC-NATO-MAN 4"
	CWAY.DisplayName = _("VPC-NATO-MAN 4")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000201";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,
                "Infantry",
					};
					
	CWAY.category = "Infantry";
	
	add_surface_unit(CWAY)	
	
	
	
-- CWAY - Fudges Cone Barricade X3
	CWAY = {};
	set_recursive_metatable(CWAY, GT_t.generic_human)
	set_recursive_metatable(CWAY.chassis, GT_t.CH_t.HUMAN);
	CWAY.chassis.life = 10

	CWAY.visual.shape = "VPC-NATO-MAN 4 v2.EDM"
	CWAY.visual.shape_dstr = "VPC-NATO-MAN 4 v2.EDM"

	--Burning after hit
	CWAY.visual.fire_size = 0 --relative burning size
	CWAY.visual.fire_pos = {0,0,0};
	CWAY.visual.fire_time = 0 --burning time (seconds)
	CWAY.time_agony = 180;
        
	
        CWAY.Name = "VPC-NATO-MAN 4 v2"
	CWAY.DisplayName = _("VPC-NATO-MAN 4 v2")
	CWAY.Rate = 1

	CWAY.DetectionRange  = 0;
	CWAY.ThreatRange = 0;
	CWAY.mapclasskey = "P0091000201";
	CWAY.attribute = {wsType_Ground,wsType_Tank,wsType_Gun,wsType_GenericInfantry,
                "Infantry",
					};
					
	CWAY.category = "Infantry";
	
	add_surface_unit(CWAY)	
	
