mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_texture_path  (current_mod_path.."/Textures")

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1

GT.visual.shape = "U-80_Open.EDM"
GT.visual.shape_dstr = ""
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "U-80_Open"
GT.DisplayName = _("U-80 Hangar Open")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)


GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1

GT.visual.shape = "U-80_Close.EDM"
GT.visual.shape_dstr = ""
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "U-80_Close"
GT.DisplayName = _("U-80 Hangar Close")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)