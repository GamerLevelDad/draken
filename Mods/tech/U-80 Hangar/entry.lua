declare_plugin("U-80 Hangar by VPS_Tango & VPS_Spit",
{
--image     	 = "FC.bmp",
installed 	 = true, -- if false that will be place holder , or advertising
dirName	  	 = current_mod_path,
version		 = "beta",		 
state		 = "installed",
}
)
---------------------------------------------------------------------------------------
mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_texture_path (current_mod_path.."/Textures/")

dofile(current_mod_path..'/U-80.lua')

plugin_done()-- finish declaration , clear temporal data