@echo off
REM Checking if the user dropped the folder onto the file
SET SavedGames=Saved Games
SET src=%~dp0
if "%~1"=="" goto PROMPT
SET dstpath=%1
GOTO COPY

REM if not then we hit 
:PROMPT
SET dstpath=0
ECHO In order to move the Draken Files properly we must know where your SavedGames\DCS folder is
ECHO Please press the number that represents your SavedGames\DCS folder
ECHO 1. Saved Games\DCS
ECHO 2. Saved Games\DCS.openbeta
SET /P pathid="Enter your choice (1 or 2): "
IF "%pathid%"=="1" SET dstpath=\%SavedGames%\DCS
IF "%pathid%"=="2" SET dstpath=\%SavedGames%\DCS.openbeta
IF "%dstpath%"=="0" GOTO ERROR
SET dstpath=%USERPROFILE%%dstpath%
GOTO COPY

:COPY
echo Copying Mods and Liveries to: %dstpath%
pause
robocopy "%src%\Liveries" "%dstpath%\Liveries" /e 
robocopy "%src%\Mods" "%dstpath%\Mods" /e 
GOTO END

:ERROR
echo Invalid input detected make sure to ONLY put 1 or 2
GOTO END

:END
pause