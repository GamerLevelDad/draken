livery = {
	{"fuz-center", 0, "SU39-PAINT1-DEF-IRGC.dds", false},
	{"wings", 0, "SU39-PAINT2-DEF-IRGC.dds", false},
	{"SU39-PAINT2bis-DEF-01", 0, "SU39-PAINT2bis-DEF-IRGC.dds", false},
	{"fuz-tail & fin", 0, "SU39-PAINT3-DEF-IRGC.dds", false},
	{"tail & stab", 0, "SU39-PAINT4-DEF-IRGC.dds", false},
	{"brake-L", 0, "SU39-PAINT5-L-DEF-IRGC.dds", false},
	{"brake-R", 0, "SU39-PAINT5-R-DEF-IRGC.dds", false},
	{"fuz-nose-steel", 0, "SU39-PAINT6-DEF-IRGC.dds", false},
	{"SU-39-PILON", 0, "SU39-PILON-PAINT-DEF-IRGC.dds", false},
	{"PILON_2_Su-39", 0, "SU39-PILON-2-PAINT-DEF-IRGC.dds", false},
	{"numbers",0,"empty",true};
	{"PylonsSu-25T1", 0, "Su-25TPylon.bmp", true},
	{"pylon25lateral", 0, "Lateral25TPylon.bmp", true},
	{"PTB-800-PAINT", 0, "PTB-800-PAINT-DEF-IRGC.dds", false},
}
name = "Draken International Desert"
countries = {"RUS","UKR","INS", "USA", "AUSAF"}
--made by 59th_Jack 2018.