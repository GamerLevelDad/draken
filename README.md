IF YOU ARE NOT PART OF THE 16AGR DRAKEN COMMUNITY AND FOUND THIS REPO, TURN AROUND, YOU ARE IN THE WRONG PLACE

This will be the repo for holding all of the files for participating in the Draken missions

A-4E mod is NOT MY OWN WORK!
Full credit goes to: https://github.com/heclak/community-a4e-c/
This is being hosted here strictly for internal purposes of the squadron

Current Mod List: (UPDATED 5/13/20)
* A-4E-C
* L61
* 476 Range Targets
* Air Power Unit
* DRAKEN
* Navy Equipment
* RadarDome
* U-80 Hanger
* VPC Airfield Equipment
* Ze Dim New Static Pack
* TACAN Mod
* AM2 Mat
* WWII Marston Mat
* NatoGF



Current Livery List:
* Draken AV-8B
* Draken C-130
* Draken JF-17
* L61
* Draken SU-25T

Installation Instructions

There are 2 ways to interact with this repository
1. Download the .zip of the master branch and unzip it to any directory which is not in your DCS folders
2. Download and install git, then setup an empty folder on disk to pull down the repo

Instructions for Option 1
1. Navigate to the gitlab repo on the web
2. Download the .zip file
3. Extract the .zip file anywayer OUTSIDE of the DCS directories
4. Drag your UserProfile\Saved Games\DCS folder ONTO the add_mods_to_dcs.bat 
5. If that doesn't work or is confusing, run the bat and follow the directions on screen.

Instructions for Option 2
1. Download and install GIT for windows
2. I'd recommend using the Windows command prompt instead of PuTTY (personal pref though)
3. Create an empty directory anywhere OUTSIDE of the DCS directories
4. Open the directory in Windows Explorer
5. Right click in the directory and select Git BASH
6. Type "git init" to setup git in the directory
7. Type git remote add -t master draken https://gitlab.com/GamerLevelDad/draken
8. Type git pull draken (this will take some time) *** Note here you may need to login with git credentials ***
9. After the download completes, CLOSE the git BASH interface
10. Return to Windows Explorer
11. Drag your UserProfile\Saved Games\DCS folder ONTO the add_mods_to_dcs.bat 
12. If that doesn't work or is confusing, run the bat and follow the directions on screen.
13. To update the repo, run the pull_latest_git.bat then enter the tag for the release for the mission


Foot notes / Thoughts
The reason why you will extract / pull into a directory outside of DCS is git adds extra hidden folders / files that may or may not affect DCS World.  Without extensive testing that may or may not break installations, this method is recommended.  I have spoken.


